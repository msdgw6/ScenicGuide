package org.gl.customview;

import android.graphics.Rect;

public interface MultiTouchEventListener {

	void scale(Rect rect, float newDistance, float oldDistance, float x1,
			float y1, float x_current, float y_current, float x2_current,
			float y2_current, int scale_speed);

	void scroll(Rect rect, float x, float y, int x2, int y2, int scroll_speed,
			int scroll_speed2);

	void scrollStop();

	void ScaleStop();

}
