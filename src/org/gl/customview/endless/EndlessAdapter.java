/***
  Copyright (c) 2008-2009 CommonsWare, LLC
  Portions (c) 2009 Google, Inc.
  
  Licensed under the Apache License, Version 2.0 (the "License"); you may
  not use this file except in compliance with the License. You may obtain
  a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package org.gl.customview.endless;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Adapter that assists another adapter in appearing endless. For example, this
 * could be used for an adapter being filled by a set of Web service calls,
 * where each call returns a "page" of data.
 * 
 * Subclasses need to be able to return, via getPendingView() a row that can
 * serve as both a placeholder while more data is being appended.
 * 
 * The actual logic for loading new data should be done in appendInBackground().
 * This method, as the name suggests, is run in a background thread. It should
 * return true if there might be more data, false otherwise.
 * 
 * If your situation is such that you will not know if there is more data until
 * you do some work (e.g., make another Web service call), it is up to you to do
 * something useful with that row returned by getPendingView() to let the user
 * know you are out of data, plus return false from that final call to
 * appendInBackground().
 */
abstract public class EndlessAdapter extends BaseAdapter {
	/**
	 * call this function by AsyncTask
	 * 
	 * @param oldList
	 * @return
	 */
	abstract protected List<?> wantLoadMoreData(List<?> oldList);

	private View pendingView = null;
	private Context context;
	private int pendingResource = -1;
	private int itemViewResourceID = -1;
	/**
	 * 原子钟 超越代码的赋值速度。避免同步造成的UI卡顿
	 */
	private AtomicBoolean isLoadMoreing = new AtomicBoolean(true);
	private List<?> datalist;
	private ListView listView;
	private Animation loadMoreAnimation = null;
	private LayoutInflater inflater;

	/**
	 * Constructor wrapping a supplied ListAdapter, providing a id for a pending
	 * view and explicitly set if there is more data that needs to be fetched or
	 * not.
	 * 
	 * @param context
	 * @param wrapped
	 * @param pendingResource
	 * @param keepOnAppending
	 */
	public EndlessAdapter(Context context, List<?> datalist,
			int loadMoreResourceID, int itemViewID, ListView listView) {
		this.context = context;
		this.datalist = datalist;
		this.pendingResource = loadMoreResourceID;
		this.itemViewResourceID = itemViewID;
		this.listView = listView;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		isLoadMoreing.set(false);
		if (pendingView == null) {
			pendingView = getPendingView(null);
			if (listView.getFooterViewsCount() == 0) {
				listView.addFooterView(pendingView);
				listView.postInvalidate();
			}
		}
	}
	public int selectId;

	public void requestSelectEffect(int id) {
		selectId = id;
		notifyDataSetChanged();
	};
	/**
	 * set the footView Animation ,the Animation should be setRepeatCount(Animation.INFINITE)
	 * @param loadMoreAnimation
	 */
	public void setLoadMoreAnimation(Animation loadMoreAnimation) {
		this.loadMoreAnimation = loadMoreAnimation;
		pendingView.clearAnimation();
		pendingView.startAnimation(loadMoreAnimation);
	}

	/**
	 * How many items are in the data set represented by this Adapter.
	 */
	@Override
	public int getCount() {

		return (datalist == null) ? 0 : datalist.size();
	}

	@Override
	public Object getItem(int position) {
		return (listView == null) ? null : listView.getChildAt(position);
	}

	/**
	 * return 0
	 */
	@Override
	@Deprecated
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public abstract View wantListViewItem(int position, View convertView,
			ViewGroup parent);

	/**
	 * Get a View that displays the data at the specified position in the data
	 * set. In this case, if we are at the end of the list and we are still in
	 * append mode, we ask for a pending view and return it, plus kick off the
	 * background task to append more data to the wrapped adapter.
	 * 
	 * @param position
	 *            Position of the item whose data we want
	 * @param convertView
	 *            View to recycle, if not null
	 * @param parent
	 *            ViewGroup containing the returned View
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (position == (getCount() - 1) && !isLoadMoreing.get()) {
			if (listView.getFooterViewsCount() == 0) {
				listView.addFooterView(pendingView);
			}
			executeAsyncTask(new AppendTask(this, datalist));
		}
		if (itemViewResourceID <= 0) {
			TextView view = new TextView(context);
			view.setText(" null " + position);
			return view;
		}
		if (convertView == null) {
			convertView = inflater.inflate(itemViewResourceID, null);
		}
		return wantListViewItem(position, convertView, null);
//		return getPendingView(parent);
	
	}

	@TargetApi(11)
	private <T> void executeAsyncTask(AsyncTask<T, ?, ?> task, T... params) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
		} else {
			task.execute(params);
		}
	}

	/**
	 * A background task that will be run when there is a need to append more
	 * data. Mostly, this code delegates to the subclass, to append the data in
	 * the background thread and rebind the pending view once that is done.
	 */
	class AppendTask extends AsyncTask<Void, Void, List<?>> {
		EndlessAdapter adapter = null;
		List<?> mOldList = null;
		int oldSize ;
		protected AppendTask(EndlessAdapter adapter, List<?> mList) {
			this.adapter = adapter;
			this.mOldList = mList;
			this.oldSize = mList.size();
		}

		@Override
		protected void onPreExecute() {
//			pendingView.setVisibility(View.VISIBLE);
			isLoadMoreing.set(true);
			// adapter.wantLoadMoreData(list);
			System.out.println("onPreExecute");

			super.onPreExecute();
		}

		@Override
		protected List<?> doInBackground(Void... params) {
			List<?> list = adapter.wantLoadMoreData(mOldList);
			System.out.println("doInBackground" + list.size());
			return list;
		}

		@Override
		protected void onProgressUpdate(Void... values) {

			System.out.println("onProgressUpdate");
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(List<?> list) {
			System.out.println("onPostExecute");
			if (list.size()>oldSize) {
				notifyDataSetChanged();
			}
			listView.removeFooterView(pendingView);
			//因为ListView 会重复调用getView方法。为了防止LISTVIEW没有加载到更多数据且停留在底部 循环触发这个方法
			//这里延迟了50毫秒设置isLoadMoreing的值为false 以此错过GETVIEW的调用时间段
			handler.postDelayed(mRunnable_Delayed_refrush,50);
			
		}
	}
	Handler handler = new Handler();
	/**
	 * 设置 为false
	 */
	Runnable mRunnable_Delayed_refrush = new Runnable() {
		
		@Override
		public void run() {
			isLoadMoreing.set(false);
		}
	};
	/**
	 * Inflates pending view using the pendingResource ID passed into the
	 * constructor
	 * 
	 * @param parent
	 * @return inflated pending view, or null if the context passed into the
	 *         pending view constructor was null.
	 */
	protected View getPendingView(ViewGroup parent) {
		if (context != null) {

			View view = null;
			if (pendingResource > 0) {
				view = inflater.inflate(pendingResource, parent, false);
			} else {
				view = new ImageView(context);
				view.setBackgroundResource(android.R.drawable.stat_notify_sync);
			}

			if (loadMoreAnimation == null) {
				loadMoreAnimation = new RotateAnimation(0f, 960f,
						Animation.RELATIVE_TO_SELF, 0.5f,
						Animation.RELATIVE_TO_SELF, 0.5f);
				loadMoreAnimation.setDuration(1000);
				loadMoreAnimation.setRepeatMode(Animation.RESTART);
				loadMoreAnimation.setRepeatCount(Animation.INFINITE);
			}
			view.startAnimation(loadMoreAnimation);
			return view;
		}

		throw new RuntimeException(
				"You must either override getPendingView() or supply a pending View resource via the constructor");
	}

	/**
	 * Getter method for the Context being held by the adapter
	 * 
	 * @return Context
	 */
	protected Context getContext() {
		return (context);
	}
	public void demo(){
//		ListView listvew = new ListView(this);
//		List<Integer> mlist = new ArrayList<Integer>();
//		for (int i = 0; i < 50; i++) {
//			mlist.add(i);
//		}
//		listvew.setAdapter(new EndlessAdapter(this, mlist, R.layout.sss, 0, listvew) {
//
//			@Override
//			protected List<?> wantLoadMoreData(List<?> oldList) {
//				
//				try {
//					Thread.currentThread().sleep(1000);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				if (oldList.size()>100) {
//					return oldList;	
//				}
//				for (int i = 0; i < 20; i++) {
//					((ArrayList<Integer>)oldList).add(i);
//				}
//				return oldList;
//			}
//
//			@Override
//			public View wantListViewItem(int position, View convertView,
//					ViewGroup parent) {
//				convertView.findViewById(R.id.imageView2);
//				return convertView;
//			}
//		});
	}
}
