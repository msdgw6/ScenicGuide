package org.gl.customview.sildselect;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.RelativeLayout;
import android.widget.Scroller;

import com.chuangxu.tourguide.ui.R;

/**
 * TODO: document your custom view class.
 */
public class SildSelectView extends RelativeLayout {

	private TextPaint mTextPaint;
	View mSilderContrlView;
	View mContrlLinerView;
	View mContrlLinerView_bottom;
	View mContrlRebootView;
	View mContrlShutdownrView;
	AnimationDrawable SmilderContrlViewAnimationDrawable;
	AnimationDrawable mContrlLinerViewAnimationDrawable;
	AnimationDrawable mContrlLinerView_bottomAnimationDrawable;
	AnimationDrawable mContrlRebootViewAnimationDrawable;
	AnimationDrawable mContrlShutdownrViewAnimationDrawable;
	int screenWidth;
	int screenHeight;
	Scroller mScroller;
	SelectListener mSelectListener;
	AnimationDrawable selectedAnimationDrawable;

	public SildSelectView(Context context) {
		super(context);
		init(null, 0);
	}

	public SildSelectView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs, 0);
	}

	public SildSelectView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs, defStyle);
	}

	private void init(AttributeSet attrs, int defStyle) {
		// Load attributes
		final TypedArray a = getContext().obtainStyledAttributes(attrs,
				R.styleable.SildSelectView, defStyle, 0);
		a.recycle();

		// Set up a default TextPaint object
		mTextPaint = new TextPaint();
		mTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
		mTextPaint.setTextAlign(Paint.Align.LEFT);

		// Update TextPaint and text measurements from attributes
		invalidateTextPaintAndMeasurements();
		DisplayMetrics dm = getResources().getDisplayMetrics();
		screenWidth = dm.widthPixels;
		screenHeight = dm.heightPixels;
		mScroller = new Scroller(getContext(), new OvershootInterpolator());
		selectedAnimationDrawable = (AnimationDrawable) getResources()
				.getDrawable(R.drawable.shadowbg_touch_animlist);
	}

	Thread animathread;
	final int MSG_WHAT_ONLAYOUT = 1;
	final int MSG_WHAT_ONLAYOUTTOCENTER = 2;
	int mSilderContrlView_Center_left = 0;
	int mSilderContrlView_Center_top = 0;
	int mSilderContrlView_Center_right = 0;
	int mSilderContrlView_Center_bottom = 0;
	int mSilderContrlView_Center_x = 0;
	int mSilderContrlView_Center_y = 0;
	int mContrlRebootView_center_x = 0;
	int mContrlRebootView_center_y = 0;
	int mContrlShutdownrView_center_x = 0;
	int mContrlShutdownrView_center_y = 0;
	/**
	 * 出发控制的距离
	 */
	int Touch_distance = 0;
	Handler handler = new Handler() {
		public void dispatchMessage(android.os.Message msg) {
			switch (msg.what) {
			case MSG_WHAT_ONLAYOUT:
				int[] temp = (int[]) msg.obj;
				mSilderContrlView.layout(temp[0], temp[1], temp[2], temp[3]);
				break;
			case MSG_WHAT_ONLAYOUTTOCENTER:
				mSilderContrlView.layout(mSilderContrlView_Center_left,
						mSilderContrlView_Center_top,
						mSilderContrlView_Center_right,
						mSilderContrlView_Center_bottom);
				break;
			default:
				break;
			}
		};
	};

	class AnimaThread extends Thread {
		int lastX, lastY;

		@Override
		public void run() {
			try {
				boolean isrun = true;
				while (isrun) {
					if (mScroller != null && !mScroller.isFinished()
							&& mScroller.computeScrollOffset()) {
						isrun = true;
						if (mSilderContrlView != null) {
							View v = mSilderContrlView;
							if (lastX == 0 || lastY == 0) {
								lastY = mScroller.getCurrY();
								lastX = mScroller.getCurrX();
								continue;
							}
							int dx = (int) mScroller.getCurrX() - lastX;
							int dy = (int) mScroller.getCurrY() - lastY;
							System.out.println("mScroller:"
									+ mScroller.getCurrX() + ":"
									+ mScroller.getCurrY());
							System.out.println("last" + lastX + ":" + lastY);
							int left = v.getLeft() + dx;
							int top = v.getTop() + dy;
							int right = v.getRight() + dx;
							int bottom = v.getBottom() + dy;
							Message msg = new Message();
							msg.what = MSG_WHAT_ONLAYOUT;
							int[] i = new int[] { left, top, right, bottom };
							msg.obj = i;
							handler.removeCallbacksAndMessages(null);
							handler.sendMessage(msg);

							Thread.sleep(20);

							lastX = (int) mScroller.getCurrX();
							lastY = (int) mScroller.getCurrY();
						}
					} else {
						Message msg = new Message();
						msg.what = MSG_WHAT_ONLAYOUTTOCENTER;
						handler.removeCallbacksAndMessages(null);
						handler.sendMessage(msg);
						isrun = false;// FIXME 误差
						Thread.sleep(20);
					}

				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();

	}

	public void setSelectListener(SelectListener mSelectListener) {
		this.mSelectListener = mSelectListener;
	}

	boolean isinitContorl = false;

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		// TODO Auto-generated method stub
		super.onLayout(changed, l, t, r, b);
		System.out.println("onLayout");
		if (!isinitContorl) {
			initControl();
			isinitContorl = true;
		}

	}

	private void initControl() {
		mSilderContrlView = this.findViewById(R.id.contrler);
		if (mSilderContrlView != null) {
			// mSilderContrlView.setBackgroundResource(R.drawable.shadowbg_touch_animlist);
			SmilderContrlViewAnimationDrawable = (AnimationDrawable) getResources()
					.getDrawable(R.drawable.shadowbg_touch_animlist);
			mSilderContrlView
					.setBackgroundDrawable(SmilderContrlViewAnimationDrawable);
			SmilderContrlViewAnimationDrawable = (AnimationDrawable) mSilderContrlView
					.getBackground();
			mSilderContrlView.setOnTouchListener(new View.OnTouchListener() {
				int lastX, lastY;
				boolean isInit = false;

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						if (!isInit) {
							initViewXY(v);
							isInit = !isInit;
						}
						SmilderContrlViewAnimationDrawable.stop();
						SmilderContrlViewAnimationDrawable.start();
						lastX = (int) event.getRawX();
						lastY = (int) event.getRawY();
						mContrlRebootViewAnimationDrawable.stop();
						mContrlShutdownrViewAnimationDrawable.stop();
						mContrlLinerViewAnimationDrawable.stop();
						mContrlLinerView_bottomAnimationDrawable.stop();
						if (mScroller.isFinished()) {
							mScroller.abortAnimation();
						}
						return true;
					case MotionEvent.ACTION_MOVE:
						int dx = (int) event.getRawX() - lastX;
						int dy = (int) event.getRawY() - lastY;

						int left = v.getLeft() + dx;
						int top = v.getTop() + dy;
						int right = v.getRight() + dx;
						int bottom = v.getBottom() + dy;

						if (left < 0) {
							left = 0;
							right = left + v.getWidth();
						}

						if (right > screenWidth) {
							right = screenWidth;
							left = right - v.getWidth();
						}

						if (top < 0) {
							top = 0;
							bottom = top + v.getHeight();
						}

						if (bottom > screenHeight) {
							bottom = screenHeight;
							top = bottom - v.getHeight();
						}

						v.layout(left, top, right, bottom);

						lastX = (int) event.getRawX();
						lastY = (int) event.getRawY();
						View view = computerDistance(event); // FIXME
						if (view != null) {
							Drawable drawable = view.getBackground();
							if ((drawable != selectedAnimationDrawable)) {
								view.setBackgroundDrawable(selectedAnimationDrawable);
								((AnimationDrawable) view.getBackground())
										.start();
							}

						} else {
							mContrlShutdownrView
									.setBackgroundDrawable(mContrlShutdownrViewAnimationDrawable);
							mContrlRebootView
									.setBackgroundDrawable(mContrlRebootViewAnimationDrawable);
						}
						return true;
					case MotionEvent.ACTION_UP:
						// 停止一些动画效果
						SmilderContrlViewAnimationDrawable.stop();
						mContrlRebootViewAnimationDrawable.start();
						mContrlShutdownrViewAnimationDrawable.start();
						mContrlLinerViewAnimationDrawable.start();
						mContrlLinerView_bottomAnimationDrawable.start();
						View view2 = computerDistance(event); // FIXME
						if (view2 != null) {
							view2.setBackgroundResource(R.drawable.shadowbg);
							if (mSelectListener != null) {
								mSelectListener.onSelected(view2);
							}
						} else {
							// 开启一个控制按钮回原味的动画
							mScroller.startScroll((int) event.getRawX(),
									(int) event.getRawY(), screenWidth / 2
											- (int) event.getRawX(),
									screenHeight / 2 - (int) event.getRawY(),
									1500);
							lastX = (int) event.getRawX();
							lastY = (int) event.getRawY();
							if (animathread != null && animathread.isAlive()) {
								animathread.interrupt();
							}
							animathread = new AnimaThread();
							animathread.start();
						}
						return true;
					default:
						break;
					}

					return false;
				}

			});
			SmilderContrlViewAnimationDrawable.start();
		}
		for (int i = 0; i < getChildCount(); i++) {

			final View childView = getChildAt(i);
			childView.measure(1000, 1000); // 在获取宽度和高度之前要写上这个
			final int childWidth = childView.getMeasuredWidth();
			System.out.println("w:" + childWidth);
			final int childHeight = childView.getMeasuredHeight();
			System.out.println("h:" + childHeight);
		}
		mContrlLinerView = findViewById(R.id.liner);
		final int totalFrame = 25;
		// int grap = screenHeight / 2 / totalFrame;
		// 构建颜色渐变动画
		AnimaDrawableFactory mAnimaDrawableFactory = new AnimaDrawableFactory();
		mContrlLinerViewAnimationDrawable = mAnimaDrawableFactory
				.makeHeartBeatLineAnimaDrawable(Color.GREEN, Color.TRANSPARENT,
						Color.TRANSPARENT, screenHeight / 2, totalFrame, 1);
		mContrlLinerView
				.setBackgroundDrawable(mContrlLinerViewAnimationDrawable);
		mContrlLinerViewAnimationDrawable = (AnimationDrawable) mContrlLinerView
				.getBackground();
		mContrlLinerViewAnimationDrawable.start();

		mContrlLinerView_bottom = findViewById(R.id.liner_bottom);
		mContrlLinerView_bottomAnimationDrawable = mAnimaDrawableFactory
				.makeHeartBeatLineAnimaDrawable(Color.TRANSPARENT, Color.GREEN,
						Color.GREEN, screenHeight / 2, totalFrame, 0);
		mContrlLinerView_bottom
				.setBackgroundDrawable(mContrlLinerView_bottomAnimationDrawable);
		mContrlLinerView_bottomAnimationDrawable = (AnimationDrawable) mContrlLinerView_bottom
				.getBackground();
		mContrlLinerView_bottomAnimationDrawable.start();

		mContrlRebootView = findViewById(R.id.reboot);
		mContrlShutdownrView = findViewById(R.id.shutdown);
		initViewXY(mSilderContrlView);
		int w_reboot = mContrlRebootView.getRight()
				- mContrlRebootView.getLeft();
		int h_reboot = mContrlRebootView.getBottom()
				- mContrlRebootView.getTop();
		mContrlRebootView.setBackgroundDrawable(mAnimaDrawableFactory
				.makeFireflyLinghtAnimaDrawable(w_reboot / 2, h_reboot / 2,
						0xff4EB02A, 0x00339BAB, w_reboot / 2 + 5, 25, 0));
		if (mContrlRebootView != null) {
			mContrlRebootViewAnimationDrawable = (AnimationDrawable) mContrlRebootView
					.getBackground();
			mContrlRebootViewAnimationDrawable.start();
		}

		int w_Shutdown = mContrlShutdownrView.getRight()
				- mContrlShutdownrView.getLeft();
		int h_Shutdown = mContrlShutdownrView.getBottom()
				- mContrlShutdownrView.getTop();
		mContrlShutdownrView.setBackgroundDrawable(mAnimaDrawableFactory
				.makeFireflyLinghtAnimaDrawable(w_Shutdown / 2, h_Shutdown / 2,
						0xFFAB3034, 0x00AB8574, w_Shutdown / 2 + 5, 25, 0));
		if (mContrlShutdownrView != null) {
			mContrlShutdownrViewAnimationDrawable = (AnimationDrawable) mContrlShutdownrView
					.getBackground();
			mContrlShutdownrViewAnimationDrawable.start();
		}
	}

	public void initViewXY(View v) {
		mSilderContrlView_Center_left = v.getLeft();
		mSilderContrlView_Center_top = v.getTop();
		mSilderContrlView_Center_right = v.getRight();
		mSilderContrlView_Center_bottom = v.getBottom();
		mSilderContrlView_Center_x = (mSilderContrlView_Center_right + mSilderContrlView_Center_left) / 2;
		mSilderContrlView_Center_y = (mSilderContrlView_Center_top + mSilderContrlView_Center_bottom) / 2;
		mContrlRebootView_center_x = mContrlRebootView.getLeft() / 2
				+ mContrlRebootView.getRight() / 2;
		mContrlRebootView_center_y = mContrlRebootView.getTop() / 2
				+ mContrlRebootView.getBottom() / 2;
		mContrlShutdownrView_center_x = mContrlShutdownrView.getLeft() / 2
				+ mContrlShutdownrView.getRight() / 2;
		mContrlShutdownrView_center_y = mContrlShutdownrView.getTop() / 2
				+ mContrlShutdownrView.getBottom() / 2;
		Touch_distance = mSilderContrlView_Center_right
				- mSilderContrlView_Center_left;
	}

	/**
	 * 计算滑动触发按钮和触发点的距离
	 * 
	 * @param mMotionEvent
	 *            返回出发点对象
	 * @return
	 */
	protected View computerDistance(MotionEvent mMotionEvent) {
		int x = (int) mMotionEvent.getRawX();
		int y = (int) mMotionEvent.getRawY();
		if ((Math.abs(mContrlRebootView_center_x - x) < Touch_distance)
				&& (Math.abs(mContrlRebootView_center_y - y) < Touch_distance)) {
			System.out.println("reboot" + mContrlRebootView_center_x);
			return mContrlRebootView;
		} else if ((Math.abs(mContrlShutdownrView_center_x - x) < Touch_distance)
				&& (Math.abs(mContrlShutdownrView_center_y - y) < Touch_distance)) {
			System.out.println("mContrlShutdownrView" + "x:" + x + "y:" + y
					+ ">>" + mContrlShutdownrView_center_x + ":"
					+ mContrlShutdownrView_center_y + "Touch_distance"
					+ Touch_distance);
			return mContrlShutdownrView;
		}

		return null;
	}

	@Override
	protected void onAttachedToWindow() {
		// TODO Auto-generated method stub
		super.onAttachedToWindow();
		System.out.println("onAttachedToWindow");
		if (animathread != null && animathread.isAlive()) {
			animathread.interrupt();
		}
	}

	// @Override
	// public void onScreenStateChanged(int screenState) {
	// // TODO Auto-generated method stub
	// super.onScreenStateChanged(screenState);
	// System.out.println("onScreenStateChanged");
	// }

	@Override
	protected void onAnimationEnd() {
		// TODO Auto-generated method stub
		super.onAnimationEnd();
		System.out.println("onAnimationEnd");
	}

	private void invalidateTextPaintAndMeasurements() {
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		// TODO: consider storing these as member variables to reduce
		// allocations per draw cycle.

	}

}
