package org.gl.customview.sildselect;

import android.graphics.Shader;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;

public class AnimaDrawableFactory {
	/**
	 * 这个动画类似于心电图中没有心跳的样子
	 * 
	 * @param CursorColor
	 * @param startColor
	 * @param endColor
	 * @param linerLength
	 * @param frameSize
	 * @param moveRule
	 *            移动规则
	 * @return
	 */
	public AnimationDrawable makeHeartBeatLineAnimaDrawable(int CursorColor,
			int startColor, int endColor, int linerLength, int frameSize,
			int moveRule) {
		final int totalFrame = frameSize;
		AnimationDrawable mContrlLinerViewAnimationDrawable;
		// 构建颜色渐变动画
		mContrlLinerViewAnimationDrawable = new AnimationDrawable();
		for (int i = 0; i < totalFrame; i++) {
			ShapeDrawable mContrlLinerViewDrawable = new ShapeDrawable(
					new RectShape());
			setGradient(CursorColor, startColor, endColor, linerLength,
					moveRule, totalFrame, i, mContrlLinerViewDrawable);
			mContrlLinerViewAnimationDrawable.addFrame(
					mContrlLinerViewDrawable, 100);
		}
		mContrlLinerViewAnimationDrawable.setOneShot(false);// 反复播放动画
		return mContrlLinerViewAnimationDrawable;
	}

	public void setGradient(int CursorColor, int startColor, int endColor,
			int linerLength, int moveRule, final int totalFrame, int i,
			ShapeDrawable mContrlLinerViewDrawable) {
		if (moveRule != 0) {
			mContrlLinerViewDrawable.getPaint().setShader(
					GradientFatctory.makeLinear(0, 0, 0, linerLength,
							new int[] { startColor, CursorColor, endColor },
							new float[] { 0, 0.1f * (totalFrame - i), 1.0f },
							Shader.TileMode.CLAMP));
		} else {
			// 设置颜色梯度变化的参数
			mContrlLinerViewDrawable.getPaint().setShader(
					GradientFatctory.makeLinear(0, 0, 0, linerLength,
							new int[] { startColor, CursorColor, endColor },
							new float[] { 0, 0.1f * i, 1.0f },
							Shader.TileMode.CLAMP));
		}
	}
	/**
	 * 萤火虫动画
	 * @param centerx
	 * @param centery
	 * @param centerColor
	 * @param edgeColor
	 * @param radial
	 * @param frameSize
	 * @param moveRule
	 * @return
	 */
	public AnimationDrawable makeFireflyLinghtAnimaDrawable(int centerx,
			int centery, int centerColor, int edgeColor, int radial,
			int frameSize, int moveRule) {
		final int totalFrame = frameSize;
		final int grap = radial / frameSize;
		AnimationDrawable mContrlLinerViewAnimationDrawable;
		// 构建颜色渐变动画
		mContrlLinerViewAnimationDrawable = new AnimationDrawable();
		int center = totalFrame / 2;
		for (int i = 0; i < totalFrame; i++) {
			ShapeDrawable mContrlLinerViewDrawable = new ShapeDrawable(
					new OvalShape());
			Shader mshader;
			// mContrlLinerViewDrawable.getPaint().setShader(GradientFatctory.makeRadial(centerx,
			// centery, i*grap+1, new int[]{edgeColor,edgeColor,
			// edgeColor},null, Shader.TileMode.CLAMP));
			if (i >= center) {
				mshader = GradientFatctory.makeRadial(centerx, centery, radial
						- (i - center) * grap + 1, centerColor, edgeColor,
						Shader.TileMode.CLAMP);//缩小
			} else {
				mshader = (GradientFatctory.makeRadial(centerx, centery,
						(i * grap) * 2 + 1, centerColor, edgeColor,
						Shader.TileMode.CLAMP));//放大
			}
//			Paint fgPaintSel = mContrlLinerViewDrawable.getPaint();
//		    fgPaintSel.setColor(color);
//		    fgPaintSel.setStyle(Style.STROKE);
//		    fgPaintSel.setStrokeWidth(height);
//		    fgPaintSel.setPathEffect(new DashPathEffect(new float[] { 5, 10 }, 0));
			mContrlLinerViewDrawable.getPaint().setShader(mshader);
			mContrlLinerViewAnimationDrawable.addFrame(
					mContrlLinerViewDrawable, 40);
		}
		mContrlLinerViewAnimationDrawable.setOneShot(false);// 反复播放动画
		return mContrlLinerViewAnimationDrawable;
	}

}
