package org.gl.customview.sildselect;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.LinearGradient;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.graphics.SweepGradient;

public class GradientFatctory {

	public static Shader makeSweep() {
		return new SweepGradient(150, 25, new int[] { 0xFFFF0000, 0xFF00FF00,
				0xFF0000FF, 0xFFFF0000 }, null);
	}
	/**
	 * 原型背景
	 * @param x
	 * @param y
	 * @param radius
	 * @param color_start
	 * @param color_end
	 * @param tile
	 * @return
	 */
	public static Shader makeRadial(float x, float y, float radius,
            int color_start, int color_end, TileMode tile) {
		
		return new RadialGradient(x, y, radius, color_start, color_end, tile);
	}
	public static Shader makeRadial(float x, float y, float radius,
            int colors[], float positions[], TileMode tile) {
		return new RadialGradient( x,  y,  radius,
				colors ,  positions,  tile);
	}
	/**
	 * 它除了定义开始颜色和结束颜色以外还可以定义，多种颜色组成的分段渐变效果
	 *   LinearGradient shader = new LinearGradient(startX， startY, endX, endY, new int[]{startColor, midleColor, endColor},new float[]{0 , 0.5f, 1.0f}, Shader.TileMode.CLAMP);
       其中参数new int[]{startColor, midleColor, endColor}是参与渐变效果的颜色集合，
       其中参数new float[]{0 , 0.5f, 1.0f}是定义每个颜色处于的渐变相对位置，
       这个参数可以为null，如果为null表示所有的颜色按顺序均匀的分布 
	 * @param colors The colors to be distributed along the gradient line
	 * @return
	 */
	public static Shader makeLinear(float x0, float y0, float x1, float y1,
            int colors[], float positions[], TileMode tile) {
		return new LinearGradient( x0,  y0,  x1,  y1,
                 colors,  positions, tile);
	}
	/**
	 * 开始和结束的颜色梯度渐变
	 * @param x0
	 * @param y0
	 * @param x1
	 * @param y1
	 * @param color0
	 * @param color1
	 * @param tile
	 * @return
	 */
	public static Shader makeLinear(float x0, float y0, float x1, float y1,
            int color0, int color1, TileMode tile) {
		return new LinearGradient( x0,  y0,  x1,  y1,
                 color0,  color1, tile);
	}

	public static Shader makeTiling() {
		int[] pixels = new int[] { 0xFF00FF00, 0xFF0000FF, 0xFFFF0000, 0 };
		Bitmap bm = Bitmap.createBitmap(pixels, 2, 2, Bitmap.Config.ARGB_8888);

		return new BitmapShader(bm, Shader.TileMode.REPEAT,
				Shader.TileMode.REPEAT);
	}

}
