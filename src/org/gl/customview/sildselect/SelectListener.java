package org.gl.customview.sildselect;

import android.view.View;

public interface SelectListener {
	public void onSelected(View view);
}
