package org.gl.activity;

import static com.nostra13.example.universalimageloader.Constants.IMAGES;

import java.io.File;

import org.gl.GlobalVariable;
import org.gl.customview.Transition3d;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.chuangxu.tourguide.data.ScenicsDataSources;
import com.chuangxu.tourguide.ui.AboutActivity;
import com.chuangxu.tourguide.ui.ImageAdapter;
import com.chuangxu.tourguide.ui.IntroduceActivity;
import com.chuangxu.tourguide.ui.MapActivity;
import com.chuangxu.tourguide.ui.NoticeActivity;
import com.chuangxu.tourguide.ui.R;
import com.example.osmtest.MainActivity;
import com.nostra13.example.universalimageloader.Constants.Extra;
import com.nostra13.example.universalimageloader.ImageListActivity;

/**
 * 底部标签栏tabview
 * 
 * @author zhaoshuming
 * @date 2012-6-12
 */
@SuppressWarnings("deprecation")
public class MenuBarActivityGroup extends ActivityGroup {
	private int currentView = -1;
	private LinearLayout pageContainer = null;
	private AdapterView gv_tabPage; // 顶部Tab标签
	private ImageAdapter imageAdapter;

	private Intent[] intents = new Intent[5];; // 页面跳转Intent
	private View[] subPageView = new View[5]; // 子页面视图View
	private Integer[] tabImages = { R.drawable.icon_zhibozhongxin,
			 R.drawable.icon_zhiyuandianbo,R.drawable.icon_yuanchengguanli,
			R.drawable.icon_notice, R.drawable.icon_about // tab标签图标
	};
	private Integer[] tabImages_sel = { R.drawable.icon_zhibozhongxin_select,
			R.drawable.icon_zhiyuandianbo_select,R.drawable.icon_yuanchengguanli_select, 
			R.drawable.icon_notice_select, R.drawable.icon_about_select // tab标签图标
	};
	private Transition3d t3d;
	@SuppressLint("SdCardPath")
	public final String UriSDcard = "file:///sdcard/"+GlobalVariable.AppDir+ File.separator;
	public final String UriAsset = "assets://";
	protected JSONObject dataStruct = ScenicsDataSources.getdata();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.rnbs_main);
		GlobalVariable.getInstance().addActivity(this);
		// baseActivity.addNetStrictMode();
		initView();
		// gv_tabPage.setNumColumns(1);// 设置列数
		if (gv_tabPage instanceof GridView) {
			((GridView) gv_tabPage).setSelector(R.drawable.select_listview_item);//
		}
		// 选中的时候为透明色
		// gv_tabPage.setDrawSelectorOnTop(true);
		// gv_tabPage.setGravity(Gravity.CENTER);// 位置居中
		// gv_tabPage.setVerticalSpacing(0);// 垂直间隔
		imageAdapter = new ImageAdapter(this, tabImages, tabImages_sel,
				GlobalVariable.disPlay_w / tabImages.length,
				GlobalVariable.disPlay_w / tabImages.length); // 创建图片适配器，传递图片所需高和宽
		// imageAdapter = new ImageAdapter(this, tabImages, tabImages_sel, 76,
		// 62); // 创建图片适配器，传递图片所需高和宽
		
		gv_tabPage.setAdapter(imageAdapter);// 设置菜单Adapter
		gv_tabPage.setLayoutParams(new LinearLayout.LayoutParams(GlobalVariable.disPlay_w , GlobalVariable.disPlay_w / tabImages.length));
//		gv_tabPage.setScrollY(2);
		gv_tabPage.setOnItemClickListener(new ItemClickEvent()); // 注册点击事件
		SwitchPage(0);// 默认打开第0页
	}

	/**
	 * 实例化view
	 */
	private void initView() {
		pageContainer = (LinearLayout) findViewById(R.id.pageContainer);
		gv_tabPage = (AdapterView<?>) findViewById(R.id.gv_tabPage);
	}

	/**
	 * 底部按钮点击事件
	 */
	class ItemClickEvent implements OnItemClickListener {

		public void onItemClick(final AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			if (arg2 != currentView) {
				SwitchPage(arg2); // arg2表示选中的Tab标签号，从0~4
				currentView = arg2;
				arg0.setClickable(false);
				arg0.postDelayed(new Runnable() {

					@Override
					public void run() {
						arg0.setClickable(true);
					}
				}, 2000);
			}

		}
	}

	/**
	 * 根据ID打开指定的PageActivity
	 * 
	 * @param id
	 *            选中项的tab序号
	 */
	public void SwitchPage(int id) {
		imageAdapter.setFocus(id);
		View pageView = null;
		pageView = getPageView(id);
		if (currentView >= 0 && subPageView[currentView] != null
				&& currentView != id && subPageView[id] != null) {
			// 装载子页面View到LinearLayout容器里面
			if (t3d == null) {
				t3d = new Transition3d();
			}
//			t3d.applyRotation(subPageView[currentView], pageView, 0, 0, 90);
		}
		pageContainer.removeAllViews();// 必须先清除容器中所有的View
		pageContainer.addView(pageView, LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT);

	}

	/**
	 * 用于获取intent和pageView， 类似于单例模式，使得对象不用重复创建，同时，保留上一个对象的状态
	 * 当重新访问时，仍保留原来数据状态，如文本框里面的值。
	 * 
	 * @param pageID
	 *            选中的tab序号（0~4）
	 * @return
	 */
	private View getPageView(int pageID) {
		// if(intents == null) {
		switch (pageID) {
		case 0:
			if (intents[0] == null) {
				intents[0] = new Intent(this, MainActivity.class);// TODO
				subPageView[pageID] = getLocalActivityManager().startActivity(
						"subPageView" + pageID, intents[pageID]).getDecorView();
			}
			break;
		case 1:
			if (intents[1] == null) {
				intents[1] = new Intent(this, IntroduceActivity.class);
				intents[1].putExtra(Extra.IMAGES, IMAGES);
				subPageView[pageID] = getLocalActivityManager().startActivity(
						"subPageView" + pageID, intents[pageID]).getDecorView();
			}
			break;
		case 2:
			if (intents[2] == null) {
				intents[2] = new Intent(this, ImageListActivity.class);// FIXME
				if (dataStruct != null) {
					int spotsCouunt;
					try {
						JSONArray sopts = dataStruct.getJSONArray("spots");
						spotsCouunt = sopts.length();
						String[] stirng = new String[spotsCouunt];
						for (int i = 0; i < stirng.length; i++) {
							stirng[i]= UriAsset+sopts.getJSONObject(i).optString("icon");
						}
						intents[2].putExtra(Extra.IMAGES, stirng);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				subPageView[pageID] = getLocalActivityManager().startActivity(
						"subPageView" + pageID, intents[pageID]).getDecorView();
			}
			break;
		case 3:
			if (intents[3] == null) {
				intents[3] = new Intent(this, NoticeActivity.class);// FIXME
				subPageView[pageID] = getLocalActivityManager().startActivity(
						"subPageView" + pageID, intents[pageID]).getDecorView();
			}
			break;
		case 4:
			if (intents[4] == null) {
				intents[4] = new Intent(this, AboutActivity.class);// FIXME
				subPageView[pageID] = getLocalActivityManager().startActivity(
						"subPageView" + pageID, intents[pageID]).getDecorView();
			}
			break;
		default:
			if (intents[0] == null) {
				intents[0] = new Intent(this, MapActivity.class);
				subPageView[pageID] = getLocalActivityManager().startActivity(
						"subPageView" + pageID, intents[pageID]).getDecorView();
			}
			break;
		}

		return subPageView[pageID];
	}

	@Override
	public void onBackPressed() {

		new AlertDialog.Builder(this)
				.setMessage(getResources().getString(R.string.tips_logout))
				.setPositiveButton(
						getResources().getString(R.string.dialog_yes),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								GlobalVariable.getInstance().exit();
							}
						})
				.setNegativeButton(
						getResources().getString(R.string.dialog_no),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.cancel();
							}
						})
				.setTitle(getResources().getString(R.string.system_exit))
				.show();

	}
}
