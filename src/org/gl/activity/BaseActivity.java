package org.gl.activity;

import org.gl.GlobalVariable;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.widget.Toast;

import com.chuangxu.tourguide.data.ScenicsDataSources;
import com.chuangxu.tourguide.ui.R;

/**
 * activity基类
 * 
 * @author zhaoshuming
 * @date 2012-6-11
 */
public class BaseActivity extends Activity implements
		ScenicsDataSources.DateSourcesObserver {
	public Context mContext;
	public LayoutInflater mLayoutInflater;
	public ProgressDialog mProgressDialog;
	private int taskCount = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		GlobalVariable.getInstance().addActivity(this);
		mContext = this;
		// addNetStrictMode();
		mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	/**
	 * 解决android3.0网络访问受限问题
	 */
	public void addNetStrictMode() {
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
				.detectDiskReads().detectDiskWrites().detectNetwork() // or
																		// .detectAll()
																		// for
																		// all
																		// detectable
																		// problems
				.penaltyLog().build());
		StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
				.detectLeakedSqlLiteObjects().detectLeakedClosableObjects()
				.penaltyLog().penaltyDeath().build());
	}

	public void getData_Start() {
		if (mProgressDialog == null) {
			mProgressDialog = new ProgressDialog(mContext);
		}
		if (taskCount <= 0) {
			mProgressDialog.show();
			mProgressDialog.setContentView(R.layout.login_progress_waiting);
			// mProgressDialog.getWindow().setWindowAnimations(R.style.dialogWindowAnim);
		}
		taskCount++;

	}
	@Override
	public void getData_Complete(Object result, int taskId) {
		taskCount--;
		if (taskCount <= 0 && mProgressDialog != null) {
			mProgressDialog.dismiss();
		}
		// System.out.println("getData_Complete"+taskId+"==="+result);
	}
	@Override
	public void getData_ERR(int errCode, Object message, int taskId) {
		taskCount--;
		if (taskCount <= 0 && mProgressDialog != null) {
			mProgressDialog.dismiss();
		}
		Toast.makeText(mContext, "连接服务器出现异常", Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (getParent() != null) {
			getParent().onBackPressed();
		} else {
			super.onBackPressed();
		}
	}
	/**
	 * 退出系统提示
	 */
	/*
	 * @Override public boolean onKeyDown(int keyCode, KeyEvent event) { if
	 * (keyCode == KeyEvent.KEYCODE_BACK) { new AlertDialog.Builder(this)
	 * .setMessage(getResources().getString(R.string.tips_logout))
	 * .setPositiveButton( getResources().getString(R.string.dialog_yes), new
	 * DialogInterface.OnClickListener() {
	 * 
	 * @Override public void onClick(DialogInterface dialog, int which) {
	 * BaseActivity.this.finish(); } }) .setNegativeButton(
	 * getResources().getString(R.string.dialog_no), new
	 * DialogInterface.OnClickListener() {
	 * 
	 * @Override public void onClick(DialogInterface dialog, int which) {
	 * dialog.cancel(); } })
	 * .setTitle(getResources().getString(R.string.system_exit)) .show(); return
	 * true; } return super.onKeyDown(keyCode, event); }
	 */
}
