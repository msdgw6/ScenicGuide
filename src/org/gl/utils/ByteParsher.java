package org.gl.utils;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Type;

/**
 * 解析一个byte数组 并将其转化位一个java类 实例.
 * <p>
 * java类成员只支持基本数据类型 和字节数组.数组的长度必须在定义的时候声明
 * 
 * @author zyfgeliang
 * @deprecated 因为getFields方法返回的类成员不能保证顺序.所以.赋值上有偏差
 * @param <T>
 */
public class ByteParsher<T> {
	public T parshByte(byte bytes[], Class<T> className){
		if (bytes == null) {
			return null;
		}
		T t;
		try {
			t = (T) className.newInstance();
			Field[] fields = className.getFields();
			long cursor = 0;
			for (int i = 0; i < fields.length; i++) {
				Type type = fields[i].getType();
				int length = getDataTypeLength(type, fields[i], t,bytes);
				byte[] temp =new byte[length];
				System.arraycopy(bytes, (int) cursor, temp, 0, length);
				if (type.equals(int.class)) {
					fields[i].set(t, byteArrayToInt(temp));
				} else if (type.equals(float.class)) {
				} else if (type.equals(Double.class)) {
				} else if (type.equals(long.class)) {
					fields[i].set(t, byteArrayToLong(temp));
				} else if (type.equals(boolean.class)) {
				} else if (type.equals(byte.class)) {
				} else if (type.equals(boolean.class)) {
				} else if (type.toString().equals("class [B")) {//byte形数组
					fields[i].set(t, temp);
					System.out.println( new String(temp,"GBK"));
				} 
				cursor+=length;
				System.out.println(fields[i].getName()+",length:"+length+" ,value:"+fields[i].get(t));
			}

		} catch (InstantiationException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return null;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return t;
	}

	private int byteArrayToInt(byte[] bytes) {
		int value = 0;
		for (int i = 0; i < 4; i++) {
			value+= bytes[i]<<i*8;
		}
		return value;
	}
	public long byteArrayToLong(byte[] bytes) {
		long value = 0;
		for (int i = 0; i < 8; i++) {
			value+= bytes[i]<<i*8;
		}
		return value;
	}

	// char 16-bit Unicode 0 Unicode 2^16-1
	// byte 8-bit -128 +127
	// short 16-bit -2^15 +2^15-1
	// int 32-bit -2^31 +2^31-1
	// long 64-bit -2^63 +2^63-1
	// float 32-bit IEEE754 IEEE754
	// double 64-bit IEEE754 IEEE754
	// void
	public int getDataTypeLength(Type fieldType, Field filed, T t,byte[] bytes)
			throws IllegalArgumentException, IllegalAccessException {
		if (fieldType.equals(int.class)) {
			return 4;
		} else if (fieldType.equals(float.class)) {
			return 4;
		} else if (fieldType.equals(Double.class)) {
			return 8;
		} else if (fieldType.equals(long.class)) {
			return 8;
		} else if (fieldType.equals(boolean.class)) {
			return 1;
		} else if (fieldType.equals(byte.class)) {
			return 1;
		} else if (fieldType.equals(boolean.class)) {
			return 1;
		} else if (fieldType.toString().equals("class [B")) {//byte形数组
			return Array.getLength(filed.get(t));
		} else {
			return 0;
		}
	}
}
