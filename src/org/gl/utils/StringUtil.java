package org.gl.utils;

import java.io.UnsupportedEncodingException;


public class StringUtil {
	/**
	 * 服务器字符错编码
	 */
	public static final String SERVER_StringEncode = "iso8859-1";
	/**
	 * 本地字符错编码
	 */
	public static final String LOCAL_StringEncode = "GBK";

	public static String encodeConver(String s) {
		try {
			return new String(s.getBytes(SERVER_StringEncode),
					LOCAL_StringEncode);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return "***";
		}
	};
}
