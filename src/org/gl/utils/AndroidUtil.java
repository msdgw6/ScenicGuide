package org.gl.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.gl.GlobalVariable;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.chuangxu.tourguide.ui.R;

/**
 * @author gl
 * @date 2012-11-13
 */
public class AndroidUtil {
	/**
	 * 生成一个AlertDialog 并显示
	 * 
	 * @param content
	 *            应该是一个隶属于activity的上下文 非baseContent or applicationContent
	 * @param message
	 *            显示的消息内容
	 * @param title
	 *            显示的消息标题
	 * @param yesOnclickImp
	 *            一个按下确定按钮的实现事件
	 */
	public static void genDialog(Context content, String message, String title,
			DialogInterface.OnClickListener yesOnclickImp) {
		new AlertDialog.Builder(content)
				.setMessage(message)
				.setPositiveButton(
						content.getResources().getString(R.string.dialog_yes),
						yesOnclickImp)
				.setNegativeButton(
						content.getResources().getString(R.string.dialog_no),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.cancel();
							}
						}).setTitle(title).show();
	}

	/**
	 * 像素转逻辑密度
	 * 
	 * @param px
	 * @return
	 */
	public static int pxToDip(float px) {
		final float scale = GlobalVariable.applicationContext.getResources()
				.getDisplayMetrics().density;
		DebugUtil.println("设备显示密度density：" + scale);
		return (scale != 0) ? (int) (px / scale + 0.5f)
				: (int) (px / 1.5 + 0.5f);

	}

	/**
	 * 逻辑密度转像素
	 * 
	 * @param dip
	 * @return
	 */
	public static int dipToPx(float dip) {
		final float scale = GlobalVariable.applicationContext.getResources()
				.getDisplayMetrics().density;
		DebugUtil.outToFile("设备显示密度density：" + scale, null);
		return (scale != 0) ? (int) (dip * scale - 0.5f)
				: (int) (dip * 1.5 - 0.5f);
		// return int padding =
		// (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
		// 4,
		// GlobalVariable.applicationContext().getResources().getDisplayMetrics());
	}

	/**
	 * 检查设备网络
	 * 
	 * @return
	 */
	public static boolean checkNetwork() {
		ConnectivityManager conn = (ConnectivityManager) GlobalVariable.applicationContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo net = conn.getActiveNetworkInfo();
		if (net != null && net.isConnected()) {
			return true;
		}
		return false;
	}

	/**
	 * 通过一个24rgb字节数组创建图片并保存为图片文件
	 * 
	 * @param rgb24
	 * @param width
	 * @param height
	 * @param filename
	 * @return
	 */
	public static boolean creatPNG(byte rgb24[], int width, int height,
			String filename) {
		byte data[] = rgb24;
		try {
			Bitmap bitmap = creatBitmap(width, height, data);
			if (bitmap != null) {
				File file2 = new File(filename);
				OutputStream os = new FileOutputStream(file2);
				file2.createNewFile();
				bitmap.compress(CompressFormat.PNG, 100, os);
				bitmap.recycle();
			} else {
				System.out.println("解码失败");
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * 通过一个24rgb字节数组创建一个Bitmap
	 * 
	 * @param width
	 * @param height
	 * @param rgb24
	 * @return
	 */
	private static Bitmap creatBitmap(int width, int height, byte[] rgb24) {
		int Rgb[] = convertByteToColor(rgb24);
		Bitmap bitmap = Bitmap.createBitmap(Rgb, 0, width, width, height,
				Bitmap.Config.ARGB_8888);
		return bitmap;
	}

	/*
	 * 将24RGB数组转化为像素数组
	 */
	public static int[] convertByteToColor(byte[] data) {
		int size = data.length;
		if (size == 0) {
			return null;
		}
		// 理论上data的长度应该是3的倍数，这里做个兼容
		int arg = 0;
		if (size % 3 != 0) {
			arg = 1;
		}

		int[] color = new int[size / 3 + arg];

		if (arg == 0) { // 正好是3的倍数
			for (int i = 0; i < color.length; ++i) {

				color[i] = (data[i * 3] << 16 & 0x00FF0000)
						| (data[i * 3 + 1] << 8 & 0x0000FF00)
						| (data[i * 3 + 2] & 0x000000FF) | 0xFF000000;
			}
		} else { // 不是3的倍数
			for (int i = 0; i < color.length - 1; ++i) {
				color[i] = (data[i * 3] << 16 & 0x00FF0000)
						| (data[i * 3 + 1] << 8 & 0x0000FF00)
						| (data[i * 3 + 2] & 0x000000FF) | 0xFF000000;
			}

			color[color.length - 1] = 0xFF000000; // 最后一个像素用黑色填充
		}

		return color;
	}

	/**
	 * 在wifi未开启状态下，仍然可以获取MAC地址，但是IP地址必须在已连接状态下否则为0
	 * 
	 * @return
	 */
	public static String getLocalIP() {
		String macAddress = null, ip = null;
		WifiManager wifiMgr = (WifiManager) GlobalVariable.applicationContext
				.getSystemService(Context.WIFI_SERVICE);
		WifiInfo info = (null == wifiMgr ? null : wifiMgr.getConnectionInfo());
		if (null != info) {
			macAddress = info.getMacAddress();
			ip = int2ip(info.getIpAddress());
		}
		System.out.println("mac:" + macAddress + ",ip:" + ip);
		return ip;

	}

	public static String int2ip(long ipInt) {
		StringBuilder sb = new StringBuilder();
		sb.append(ipInt & 0xFF).append(".");
		sb.append((ipInt >> 8) & 0xFF).append(".");
		sb.append((ipInt >> 16) & 0xFF).append(".");
		sb.append((ipInt >> 24) & 0xFF);
		return sb.toString();
	}
	public static SharedPreferences getConfigSharedPreferences() {
		SharedPreferences configSharedPreferences =GlobalVariable.applicationContext. getSharedPreferences(AppConfig.SharedPreferencesXML_NAME, 0);
		return configSharedPreferences;
	}
	public static String getServerIPConfig(){
		SharedPreferences configSharedPreferences =getConfigSharedPreferences();
		return configSharedPreferences.getString(AppConfig.Secver_IP, "192.168.1.51");
	}
	public static boolean setServerIPConfig(String ip){
		SharedPreferences configSharedPreferences =getConfigSharedPreferences();
		Editor edit = configSharedPreferences.edit();
		edit .putString(AppConfig.Secver_IP, ip);
		return edit.commit();
	}
}
