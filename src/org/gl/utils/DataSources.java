package org.gl.utils;

import android.os.Handler;
import android.os.Message;

/**
 * 数据源。封装了一些android平台 异步连接服务器的操作
 * @author gl
 * @date 2012-10-29
 */
public class DataSources{
	DateSourcesObserver mDateSourcesObserver;
//	private Object LOCK = new Object();

	public void setDateSourcesObserver(
			DateSourcesObserver  mDateSourcesObserver) {
		this.mDateSourcesObserver = mDateSourcesObserver;
	}

	/**
	 * 观察数据源的状态
	 * 
	 * @author gl
	 * @date 2012-10-29
	 */
	public interface DateSourcesObserver  {
		/**
		 * 数据源开始加载数据
		 */
		public void getData_Start();

		/**
		 * 数据源开始数据完成
		 */
		public void getData_Complete( Object result, int taskId);

		/**
		 * 数据源开始加载数据出现了错误
		 */
		public void getData_ERR(int errCode, Object message, int taskId);
	}

	/**
	 * 接口，具体实现由实现类定义
	 * 
	 * @author gl
	 * @date 2012-10-29
	 */
	public interface DateSourcesTask  {
		/**
		 * 任务的执行过程
		 */
		public   Object StartTask();
	}


	/**
	 * 获取服务器上的数据，这里开启一个线程。异步加载数据
	 */
	public void getDataFromServer(final DateSourcesTask dateSourcesTask,final int taskID) {
//		System.out.println("taskName:"+dateSourcesTask.getClass().getSimpleName()+"\ntaskID:"+taskID);
		new Thread() {
			public void run() {
				sendMessage_TaskStart();
				try {
					if (dateSourcesTask != null) {
						sendMessage_TaskComplete(dateSourcesTask.StartTask(),
								taskID);
					} else {
						throw new Exception("before getDataFromServer "
								+ ",you must add one DateSourcesTask. "
								+ "or the DateSourcesTask has been clear");
					}
				} catch (Exception e) {
					e.printStackTrace();
					sendMessage_TaskERR(0, null, 0);
				} finally {
					clearTask();
				}

			};
		}.start();
		
	}

	Handler handler = new Handler() {
		@Override
		public void dispatchMessage(Message msg) {
			if (mDateSourcesObserver ==null) {
				return;
			}
			switch (msg.what) {
			case flag_task_start:
				mDateSourcesObserver.getData_Start();
				break;
			case flag_task_complete:
				mDateSourcesObserver.getData_Complete(msg.obj,
						msg.arg1);

				break;
			case flag_task_err:
					mDateSourcesObserver.getData_ERR(msg.arg2, msg.obj,
							msg.arg1);
				break;

			default:
				break;
			}
		}
	};
	private final int flag_task_start = 0;
	private final int flag_task_complete = 1;
	private final int flag_task_err = -1;

	/**
	 * 发送一个任务开始的消息 通知观察者
	 */
	public void sendMessage_TaskStart() {
		handler.sendEmptyMessage(flag_task_start);
	}

	/**
	 * 发送一个任务完成的的消息 通知观察者
	 */
	public void sendMessage_TaskComplete(Object result, int taskId) {
		Message msg = handler.obtainMessage(flag_task_complete, taskId, 0,
				result);
		handler.sendMessage(msg);
	}

	/**
	 * 发送一个任务失败的的消息 通知观察者
	 */
	public void sendMessage_TaskERR(int errCode, String message, int taskId) {
		Message msg = handler.obtainMessage(flag_task_err, taskId, errCode,
				message);
		handler.sendMessage(msg);
	}

	/**
	 * 移除任务对象
	 */
	public void clearTask() {
		//TODO
	}
}
