package org.gl.utils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
/**
 * 排序类，按照传入的对象的class类型
 * 和需要排序的String类型字段进行排序
 * 
 * 
 * @author jinfu zhou
 * @since 2011/10/19
 *
 */
public class StringSortUtil {
	/** 匹配字符串是否为纯数字的正则式 */
	private static final String numPattern = "[0-9]*";
	
	public static List sortString(List forSortList, Class clz, String orderByFieldName){
		Collections.sort(forSortList, new BaseComparator(clz, orderByFieldName));
		return forSortList;
	}
	
	private static class BaseComparator implements Comparator<Object>{
		private Class clz;
		private String fieldName;
		
		public BaseComparator(Class clz, String fieldName){
			this.clz = clz;
			this.fieldName = fieldName;
		}

		@Override
		public int compare(Object o1, Object o2) {
			try{
				Field f1 = clz.getDeclaredField(fieldName);
				Field f2 = clz.getDeclaredField(fieldName);
				f1.setAccessible(true);
				f2.setAccessible(true);
				
				String name1 = (String) f1.get(o1);
				String name2 = (String) f2.get(o2);
				
				//数字排序
				if(name1.matches(numPattern) && name2.matches(numPattern)){
					BigDecimal i1 = new BigDecimal(name1);
					BigDecimal i2 = new BigDecimal(name2);
//					long i1 = Long.parseLong(name1);
//					long i2 = Long.parseLong(name2);
					if(i1.compareTo(i2) == 0){
						//数字大小相同考虑将长度小的放在前面
						long l1 = name1.length();
						long l2 = name2.length();
						if(l1 == l2){
							return 0;//大小，长度也相同返回相等
						}else{
							return l1 > l2 ? 1 : -1;
						}
					}else {
						//大小不同，按大小排序
						return i1.compareTo(i2);
					}
				}else{
					//字母字符，汉字排序
					return Collator.getInstance(Locale.CHINA).compare(name1, name2);
				}
			}catch (Exception e){
				e.printStackTrace();
				throw new RuntimeException();
			}
		}
		
	}
}
