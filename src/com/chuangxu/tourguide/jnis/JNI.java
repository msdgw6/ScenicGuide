package com.chuangxu.tourguide.jnis;


public class JNI {
	static {
		System.loadLibrary("zyf");
		System.out.println("loadLibrary(zyf)");
	}


	/**
	 * 点播停止
	 * 
	 * @param vodHandle
	 * @return
	 */
	public static native int vodStop(int vodHandle);

	/**
	 * 当前时间点播的时钟索引
	 * 
	 * @param currentDataBlock
	 * @return
	 */
	public static native int vodFlow(int currentDataBlock);

	public static native int getLoadDataStats();

	/**
	 * 点播开始
	 * 
	 * @param addrs
	 * @param resourceId
	 * @param vodHandle
	 * @param LiveServer_port
	 *            点播服务器IP
	 * @param LiveServer_ip
	 *            点播服务器端口
	 * @return
	 */
	public static native int vodStart(int currentResID, int intHld,
			String serverVodIp, int serverVodPort, int serverVodPortUdp);

	public static native int vodPause();


	/**
	 * 通过直接接收TS流直播
	 * 
	 * @param udpport
	 *            监听的UDP端口
	 * @return
	 */
	public static native int livingTSStreamStart(String udpport);

	/**
	 * 通过直接接收TS流直播
	 * 
	 * @param udpport
	 *            监听的UDP端口
	 * @return
	 */
	public static native int livingTSStreamStop();

	/**
	 * 点播本地SPS文件
	 * 
	 * @param filepath
	 * @return
	 */
	public static native int vodFile(String filepath);

	/**
	 * 点播网络SPS文件(边下载 边播放)
	 * 
	 * @param filepath
	 * @return
	 */
	public static native int vodBufferFile(String filepath, int fileLength);

	/**
	 * 停止点播本地SPS文件
	 * 
	 * @return
	 */
	public static native int vodFileStop();
	/**
	 * 停止点播本地SPS文件
	 * 
	 * @return
	 */
	public static native int vodFilePause();

	/**
	 * 开启TCP直播  <p>
	 * roomID 直播室ID<p>
	 * streamType LIVINGTYPE_Directed ? 1 : (lMT[lMCount] ==
	 * LIVINGTYPE_Single ? 2 : 3))<p>
	 *  mediaNumber 媒体流编号 101讲 201学 301电脑
	 * 
	 * @return
	 */
	public static native int startTCPLiving(String ip, int roomID,
			int streamType, int mediaNumber);

	/**
	 * 关闭TCP直播
	 * 
	 * @return
	 */
	public static native int stopTCPLiving();

}
