package com.chuangxu.tourguide.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 加载楼层适配器，暂时无用
 * 
 * @author zhaoshuming
 * @date 2012-6-14
 */
public class ImageAdapter extends BaseAdapter {
	private Context context;
	private int width;
	private int height;
	private ViewHandler[] imageViews;

	private Integer[] tabImages;
	private Integer[] tabImages_sel;

	class ViewHandler {
		TextView tv;
		ImageView iv;
		View view;
	}
	public ImageAdapter(Context context, Integer[] tabImages,
			Integer[] tabImages_sel) {
		this(context, tabImages, tabImages_sel, 0, 0);
	}
	public ImageAdapter(Context context, Integer[] tabImages,
			Integer[] tabImages_sel, int width, int height) {
		this.context = context;
		this.tabImages = tabImages;
		this.tabImages_sel = tabImages_sel;
		this.width = width;
		this.height = height;
		imageViews = new ViewHandler[tabImages.length];
		for (int position = 0; position < imageViews.length; position++) {
			View convertView = ((LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
					.inflate(R.layout.menuitem, null);
			convertView.setLayoutParams(new GridView.LayoutParams(width, height));
			ImageView iv = (ImageView) convertView
					.findViewById(R.id.menuitem_iv);
			iv.setImageResource(tabImages[position]);
			TextView tv = (TextView) convertView.findViewById(R.id.menuitem_tv);

			switch (position) {
			case 0:
				tv.setText(context.getResources().getString(R.string.control));
				iv.setImageResource(tabImages[0]);
				break;
			case 1:
				tv.setText(context.getResources().getString(
						R.string.control_server));
				iv.setImageResource(tabImages[1]);
				break;
			case 2:
				tv.setText(context.getResources().getString(
						R.string.control_resouce));
				iv.setImageResource(tabImages[2]);
				break;
			case 3:
				tv.setText(context.getResources().getString(
						R.string.control_resouce));
				iv.setImageResource(tabImages[3]);
				break;
			case 4:
				tv.setText(context.getResources().getString(
						R.string.control_resouce));
				iv.setImageResource(tabImages[4]);
				break;

			default:
				tv.setText("no name");
				break;
			}

			ViewHandler viewHandler = new ViewHandler();
			viewHandler.tv = tv;
			viewHandler.iv = iv;
			viewHandler.view = convertView;
			imageViews[position] = viewHandler;
		}
	}

	public int getCount() {
		return tabImages.length;
	}

	public Object getItem(int position) {
		return imageViews[position];
	}

	public long getItemId(int position) {
		return position;
	}

	/**
	 * 点击设置
	 * 
	 * @param selectedID
	 */
	public void setFocus(int selectedID) {
		for (int i = 0; i < imageViews.length; i++) {
			imageViews[i].iv.setImageResource(tabImages[i]);
			imageViews[i].tv.setTextColor(context.getResources().getColor(
					R.color.grey_icon_title));
		}
		imageViews[selectedID].iv.setImageResource(tabImages_sel[selectedID]);
		imageViews[selectedID].tv.setTextColor(context.getResources().getColor(
				R.color.blue_icon_title));
		imageViews[selectedID].iv.postInvalidate();

	}

	/**
	 * 图片设置
	 */
	public View getView(int position, View convertView, ViewGroup parent) {
		// imageViews[position].setImageResource(tabImages[position]);
		// imageViews[position].setLayoutParams(new GridView.LayoutParams(width,
		// height));
		// if (convertView==null) {
		// convertView
		// =((LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.menuitem,
		// null);
		// convertView.setLayoutParams(new
		// GridView.LayoutParams(GridView.LayoutParams.WRAP_CONTENT, height));
		// }
		//
		// ImageView iv = (ImageView)
		// convertView.findViewById(R.id.menuitem_iv);
		// iv.setBackgroundResource(tabImages[position]);
		// imageViews[position] = iv;
		// System.out.println("fsadfa"+iv.hashCode());
		// TextView tv = (TextView) convertView.findViewById(R.id.menuitem_tv);
		// switch (position) {
		// case 0:
		// tv.setText(context.getResources().getString(R.string.control));
		// break;
		// case 1:
		// tv.setText(context.getResources().getString(R.string.control_server));
		// break;
		//
		// default:
		// tv.setText("no content");
		// break;
		// }
		return imageViews[position].view;
		// return convertView;
	}

}
