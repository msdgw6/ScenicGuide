package com.chuangxu.tourguide.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

public class ImageGridActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_grid);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.image_grid, menu);
		return true;
	}

}
