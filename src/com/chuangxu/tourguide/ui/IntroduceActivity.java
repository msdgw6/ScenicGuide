package com.chuangxu.tourguide.ui;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;

import uk.co.senab.photoview.sample.ViewPagerActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chuangxu.tourguide.audio.AudioPlayer;
import com.chuangxu.tourguide.audio.MediaViewHandlerI;
import com.nostra13.example.universalimageloader.BaseActivity;
import com.nostra13.example.universalimageloader.Constants.Extra;
import com.nostra13.example.universalimageloader.ImageGridActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class IntroduceActivity extends BaseActivity implements
		MediaViewHandlerI {
	private static final String STATE_POSITION = "STATE_POSITION";

	DisplayImageOptions options;

	ViewPager pager;

	private String[] imageUrls;
	AudioPlayer m;
	@Override
	public void onAttachedToWindow() {
		// TODO Auto-generated method stub
		super.onAttachedToWindow();
		System.out.println("onAttachedToWindow");
		try {
			m = new AudioPlayer(IntroduceActivity.this,
					IntroduceActivity.this, dataStruct
							.optString("audio"));
//			m.startPlayer();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	private void stopAudioPlayer() {
		if (m != null) {
			m.StopPlayer();
			m = null;
		}
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_introduce);

		Bundle bundle = getIntent().getExtras();
		imageUrls = bundle.getStringArray(Extra.IMAGES);
		int pagerPosition = bundle.getInt(Extra.IMAGE_POSITION, 0);
		if (savedInstanceState != null) {
			pagerPosition = savedInstanceState.getInt(STATE_POSITION);
		}

		if (dataStruct != null) {
			initView();
			findViewById(R.id.imageButton1).setOnClickListener(
					new View.OnClickListener() {
						@Override
						public void onClick(View v) {
//							try {
//								if (m == null) {
//									m = new AudioPlayer(IntroduceActivity.this,
//											IntroduceActivity.this, dataStruct
//													.optString("audio"));
									m.startPlayer();
//								}
//								else {
//									m.startPlayer();
//								}

//							} catch (IOException e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}

						}
					});

		}
		if (imageUrls != null) {
			options = new DisplayImageOptions.Builder()
					.showImageForEmptyUri(R.drawable.ic_empty)
					.showImageOnFail(R.drawable.ic_error)
					.resetViewBeforeLoading(true).cacheOnDisc(true)
					.imageScaleType(ImageScaleType.EXACTLY)
					.bitmapConfig(Bitmap.Config.RGB_565)
					.displayer(new FadeInBitmapDisplayer(300)).build();
			String[] bannerImageUrls = new String[9];
			System.arraycopy(imageUrls, 0, bannerImageUrls, 0, 9);
			pager = (ViewPager) findViewById(R.id.viewPager1);
			pager.setAdapter(new ImagePagerAdapter(bannerImageUrls));
			pager.setCurrentItem(pagerPosition);
			pager.setOnPageChangeListener(new OnPageChangeListener() {

				@Override
				public void onPageSelected(int arg0) {
					postion = arg0;
				}

				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {

				}

				@Override
				public void onPageScrollStateChanged(int arg0) {

				}
			});
			findViewById(R.id.icon).setOnClickListener(
					new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							startImageViewActivity(0, false);

						}

					});
		}

	}
	
	private void startImageViewActivity(int postion, boolean isGoViewPager) {
		if (dataStruct != null) {
			int spotsCouunt;
			try {
				JSONArray imagesInScenic = dataStruct.getJSONArray("images");
				spotsCouunt = imagesInScenic.length();
				String[] stirng = new String[spotsCouunt];
				for (int i = 0; i < stirng.length; i++) {
					stirng[i] = UriAsset + imagesInScenic.optString(i);
				}
				Intent i = new Intent();
				i.putExtra(Extra.IMAGES, stirng);
				i.putExtra(Extra.IMAGE_POSITION, postion);
				if (!isGoViewPager) {
					goGridViewActivity(i);
				} else {
					goImageViewPageActivity(i);
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			Toast.makeText(getApplicationContext(), "no more",
					Toast.LENGTH_SHORT).show();
		}
	}

	private void goGridViewActivity(Intent i) {
		startActivity(i.setClass(this, ImageGridActivity.class));
	}

	private void goImageViewPageActivity(Intent i) {
		startActivity(i.setClass(this, ViewPagerActivity.class));
	}

	int postion = 0;
	View.OnClickListener mOnclickImp = new OnClickListener() {

		@Override
		public void onClick(View v) {
			startImageViewActivity(postion, true);
		}
	};

	// ImageLoader imageLoader = ImageLoader.getInstance();
	/**
	 * 初始化 banner 音频源 标题 简介内容
	 */
	private void initView() {
		// ImageView icon = (ImageView) findViewById(R.id.icon);
		// imageLoader.displayImage(UriAsset + dataStruct.optString("icon"),
		// icon);
		JSONArray temp;
		try {
			temp = dataStruct.getJSONArray("images");
			int size = temp.length();
			imageUrls = new String[size];
			for (int i = 0; i < size; i++) {
				imageUrls[i] = UriAsset + temp.getString(i);
			}
			((TextView) findViewById(R.id.textView4)).setText(dataStruct
					.opt("name") + "");
			// ((TextView) findViewById(R.id.name)).setText(dataStruct
			// .optString("name"));
			// ((TextView) findViewById(R.id.name)).setText(dataStruct
			// .optString("audio"));
			((TextView) findViewById(R.id.conttext)).setText("\t\t"
					+ dataStruct.optString("content"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.introduce, menu);
		return true;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putInt(STATE_POSITION, pager.getCurrentItem());
	}

	private class ImagePagerAdapter extends PagerAdapter {

		private String[] images;
		private LayoutInflater inflater;

		ImagePagerAdapter(String[] images) {
			this.images = images;
			inflater = getLayoutInflater();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
		}

		@Override
		public void finishUpdate(View container) {
		}

		@Override
		public int getCount() {
			return images.length;
		}

		@Override
		public Object instantiateItem(ViewGroup view, int position) {
			View imageLayout = inflater.inflate(
					R.layout.item_banner_pager_image, view, false);
			ImageView imageView = (ImageView) imageLayout
					.findViewById(R.id.image);
			final ProgressBar spinner = (ProgressBar) imageLayout
					.findViewById(R.id.loading);
			imageView.setOnClickListener(mOnclickImp);
			imageView.setScaleType(ScaleType.CENTER);
			imageLoader.displayImage(images[position], imageView, options,
					new SimpleImageLoadingListener() {
						@Override
						public void onLoadingStarted(String imageUri, View view) {
							spinner.setVisibility(View.VISIBLE);
						}

						@Override
						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason) {
							String message = null;
							switch (failReason.getType()) {
							case IO_ERROR:
								message = "Input/Output error";
								break;
							case DECODING_ERROR:
								message = "Image can't be decoded";
								break;
							case NETWORK_DENIED:
								message = "Downloads are denied";
								break;
							case OUT_OF_MEMORY:
								message = "Out Of Memory error";
								break;
							case UNKNOWN:
								message = "Unknown error";
								break;
							}
							Toast.makeText(getBaseContext(), message,
									Toast.LENGTH_SHORT).show();

							spinner.setVisibility(View.GONE);
						}

						@Override
						public void onLoadingComplete(String imageUri,
								View view, Bitmap loadedImage) {
							spinner.setVisibility(View.GONE);
						}
					});

			((ViewPager) view).addView(imageLayout, 0);
			return imageLayout;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View container) {
		}
	}

	@Override
	public View getStartButton() {
		return findViewById(R.id.imageButton1);
	}

	@Override
	public View getPauseButton() {
		return findViewById(R.id.imageButton1);
	}

	@Override
	public View getStopButton() {
		return null;
	}

	@Override
	public ProgressBar getProgressBar() {
		return (ProgressBar) findViewById(R.id.progressBar1);
	}

	@Override
	public TextView getTimeTextView() {
		return (TextView) findViewById(R.id.textView3);
	}

}
