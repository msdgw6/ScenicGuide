package com.chuangxu.tourguide.ui;

import java.io.IOException;

import org.gl.activity.MenuBarActivityGroup;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.chuangxu.tourguide.resource.InitResourcer;
import com.nostra13.example.universalimageloader.BaseActivity;

public class SplashActivity extends BaseActivity {
	Bitmap bitmap;
	private TextView sp_textView1;
	Handler handler = new Handler();
	private View parentView;
	private Animation mm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		parentView = findViewById(R.id.sp_rl);
		sp_textView1 = (TextView) findViewById(R.id.sp_textView1);
		sp_textView1.setText("loading.");
		mm = AnimationUtils.loadAnimation(getBaseContext(), R.anim.sp);
		new ResouceThread().start();
	}

	class ResouceThread extends Thread {
		@Override
		public void run() {
			super.run();
			// copyAllAssetFileToSdCard();//TODO

			if (dataStruct != null) {
				try {
					Thread.sleep(500);
					handler.post(new Runnable() {
						public void run() {
							sp_textView1.setText("load image..");
						}
					});

					bitmap = BitmapFactory.decodeStream(getAssets().open(
							dataStruct.optString("splash")));

					Thread.sleep(200);
					handler.post(new Runnable() {
						@SuppressWarnings("deprecation")
						public void run() {
							sp_textView1.setText("load audio..");
							parentView
									.setBackgroundDrawable(new BitmapDrawable(
											bitmap));
						}
					});
					Thread.sleep(500);
					handler.post(new Runnable() {
						@SuppressWarnings("deprecation")
						public void run() {
							sp_textView1.setText("load map...");
							parentView
									.setBackgroundDrawable(new BitmapDrawable(
											bitmap));
						}
					});
					InitResourcer mInitResourcer = new InitResourcer();
					mInitResourcer.copyRawToSdcard(R.raw.map_db,
							InitResourcer.map_db_name);
					Thread.sleep(200);
					handler.post(new Runnable() {
						public void run() {
							sp_textView1.setText(dataStruct.optString("name"));
						}
					});
					handler.post(new Runnable() {
						public void run() {
							findViewById(R.id.sp_rl).startAnimation(mm);

						}
					});
					handler.postDelayed(new Runnable() {
						public void run() {
							startActivity(new Intent().setClass(
									SplashActivity.this,
									MenuBarActivityGroup.class));
							finish();
						}
					},mm.getDuration());
					
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				// dataStruct
			}

		}
	}

	@Override
	protected void onDestroy() {
		if (bitmap != null && !bitmap.isRecycled()) {
			bitmap.recycle();
		}
		super.onDestroy();
	}

}
