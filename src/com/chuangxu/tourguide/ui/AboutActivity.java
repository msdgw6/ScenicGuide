package com.chuangxu.tourguide.ui;

import android.os.Bundle;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.example.universalimageloader.BaseActivity;

public class AboutActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		if (dataStruct!=null) {
			((TextView)findViewById(R.id.company)).setText(dataStruct.opt("company")+"");
			((TextView)findViewById(R.id.about)).setText(dataStruct.opt("about")+"");
			((TextView)findViewById(R.id.copyright)).setText(dataStruct.optString("copyright"));
			imageLoader.displayImage(UriAsset + dataStruct.opt("company_icon"), (ImageView)findViewById(R.id.company_icon));
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.about, menu);
		return true;
	}

}
