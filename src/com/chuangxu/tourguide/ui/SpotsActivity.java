package com.chuangxu.tourguide.ui;

import org.gl.activity.BaseActivity;

import android.os.Bundle;
import android.view.Menu;
import android.widget.ListView;

public class SpotsActivity extends BaseActivity {

	private ListView listView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_spots);
		listView = (ListView) findViewById(R.id.listView1);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.spots, menu);
		return true;
	}

}
