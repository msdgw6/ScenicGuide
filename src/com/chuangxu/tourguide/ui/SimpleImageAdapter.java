package com.chuangxu.tourguide.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

/**
 * 加载楼层适配器，暂时无用
 * 
 * @author zhaoshuming
 * @date 2012-6-14
 */
public class SimpleImageAdapter extends BaseAdapter {
	private Context context;
	private int width;
	private int height;
	private ViewHandler[] imageViews;
	int size ;
	private Integer[] tabImages;
	private Integer[] tabImages_sel;

	class ViewHandler {
		ImageView iv;
		View view;
	}

	public SimpleImageAdapter(Context context, Integer[] tabImages,
			Integer[] tabImages_sel, int width, int height) {
		this.context = context;
		this.tabImages = tabImages;
		this.tabImages_sel = tabImages_sel;
		this.width = width;
		this.height = height;
		imageViews = new ViewHandler[tabImages.length];
		for (int position = 0; position < imageViews.length; position++) {
			View convertView = ((LayoutInflater) context
					.getSystemService(context.LAYOUT_INFLATER_SERVICE))
					.inflate(R.layout.grallyitem, null);
			ImageView iv = (ImageView) convertView.findViewById(R.id.iv);
			iv.setImageResource(tabImages[position]);

			ViewHandler viewHandler = new ViewHandler();
			viewHandler.iv = iv;
			viewHandler.view = convertView;
			imageViews[position] = viewHandler;
		}
		size = imageViews.length;
	}

	public int getCount() {
		return size;
	}

	public Object getItem(int position) {
		return imageViews[position];
	}

	public long getItemId(int position) {
		return position;
	}

//	/**
//	 * 点击设置
//	 * 
//	 * @param selectedID
//	 */
//	public void setFocus(int selectedID) {
//		for (int i = 0; i < imageViews.length; i++) {
//			imageViews[i].iv.setImageResource(tabImages[i]);
//		}
//		imageViews[selectedID].iv.setImageResource(tabImages_sel[selectedID]);
//		imageViews[selectedID].iv.postInvalidate();
//
//	}

	/**
	 * 图片设置
	 */
	public View getView(int position, View convertView, ViewGroup parent) {
		int index = (position%imageViews.length);
		return imageViews[index].view;
	}

	public void setData(int size) {
		this.size = size;
		notifyDataSetChanged();
	}

}
