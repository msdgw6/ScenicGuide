package com.chuangxu.tourguide.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.example.universalimageloader.BaseActivity;

public class NoticeActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notice);
		if (dataStruct != null) {
			((TextView) findViewById(R.id.textView2)).setText(dataStruct
					.optString("notice"));
			
			((TextView) findViewById(R.id.tv_titlebar_title))
					.setText(dataStruct.optString("notice_title"));
			
			if (dataStruct
					.optString("notice_title").length()>8) {
				((TextView) findViewById(R.id.tv_titlebar_title)).setTextSize(16);
			}
			
			((TextView) findViewById(R.id.textView3)).setText(dataStruct
					.optString("tel"));
			((TextView) findViewById(R.id.textView3))
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							String inputStr = dataStruct.optString("tel");
							if (inputStr.trim().length() != 0) {
								Intent phoneIntent = new Intent(
										"android.intent.action.CALL",

										Uri.parse("tel:" + inputStr));

								startActivity(phoneIntent);
							}

						}
					});
			ImageView icon = (ImageView) findViewById(R.id.imageView1);
			imageLoader.displayImage(UriAsset + dataStruct.opt("icon"), icon);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.notice, menu);
		return true;
	}

}
