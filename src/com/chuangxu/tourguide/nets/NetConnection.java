package com.chuangxu.tourguide.nets;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

 /**
  * 获取网络连接类
  */
public class NetConnection {
	private ConnectivityManager manager;
	private Context mCtxt;
	public static final String DEFAULT_APN_NAME = "internet";

	public NetConnection(Context ctx) {
		mCtxt = ctx;
		manager = (ConnectivityManager) mCtxt
				.getSystemService(Context.CONNECTIVITY_SERVICE);
	}

	public String startNetworkConnection(String curAPNName) {
		if (mCtxt == null)
			return null;
		NetworkInfo[] netInfos = manager.getAllNetworkInfo();
		String typeName = "internet";

		for (NetworkInfo netInfo : netInfos) {
			if (netInfo.getTypeName().equals("WIFI")) {

			} else if (netInfo.getTypeName().equals("MOBILE")) {
				typeName = netInfo.getSubtypeName();
				break;
			}
		}
		int result = manager.startUsingNetworkFeature(
				ConnectivityManager.TYPE_MOBILE, typeName);
		switch (result) {
		case -1:
			break;
		}
		NetworkInfo info = manager.getActiveNetworkInfo();
		typeName = info.getSubtypeName();
		return typeName;
	}
	
}
