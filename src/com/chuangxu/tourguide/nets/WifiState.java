package com.chuangxu.tourguide.nets;

import android.content.Context;
import android.net.wifi.WifiManager;

public class WifiState {
	public static boolean isWiFiConnected(Context context){
		WifiManager manager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
		int state = manager.getWifiState();
		switch(state){
			case WifiManager.WIFI_STATE_ENABLED:
			return true;
			case WifiManager.WIFI_STATE_DISABLED:
			return false;
		}
		return false;
	}
}
