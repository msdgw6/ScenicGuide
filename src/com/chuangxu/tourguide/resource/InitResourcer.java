package com.chuangxu.tourguide.resource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.gl.GlobalVariable;

import android.content.Context;
import android.content.res.AssetManager;

import com.nostra13.universalimageloader.utils.L;

public class InitResourcer {
	Context context;
	public final static String map_db_name = "map.mbtiles";

	public InitResourcer() {
		context = GlobalVariable.applicationContext;
	}

	public void copyRawToSdcard(final int rawRecourseId, final String fileName) {
		// String[] files = new String[0];
		String savedirpath = GlobalVariable.AppFileSystemDir;
		String saveFilepath = savedirpath + fileName;
		if ((new File(saveFilepath)).exists()) {
			return;
		}
		// for (String file : files) {
		try {
			InputStream is = context.getResources().openRawResource(
					rawRecourseId);
			FileOutputStream fos = new FileOutputStream(saveFilepath);
			byte[] buffer = new byte[1024];
			int read = 0;
			try {
				while ((read = is.read(buffer)) >0) {
					fos.write(buffer, 0, read);
				}
			} finally {
				fos.flush();
				fos.close();
				is.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
			L.w("Can't copy " + fileName + " into SD card");
		}
		// }
		System.out.println("cpoy finish");

	}

	/**
	 * copy本地资源到SDCARD中
	 */
	public void copyAllAssetFileToSdCard() {
		new Thread(new Runnable() {
			@Override
			public void run() {

				AssetManager am = context.getAssets();
				String[] files = new String[0];
				;
				try {
					files = am.list("");
					System.out.println(am.list("").toString());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				String savedirpath = GlobalVariable.AppFileSystemDir;
				for (String file : files) {
					try {
						InputStream is = context.getAssets().open(file);
						FileOutputStream fos = new FileOutputStream(savedirpath
								+ file);
						byte[] buffer = new byte[8192];
						int read;
						try {
							while ((read = is.read(buffer)) != -1) {
								fos.write(buffer, 0, read);
							}
						} finally {
							fos.flush();
							fos.close();
							is.close();
						}
					} catch (IOException e) {
						e.printStackTrace();
						L.w("Can't copy " + file + " image onto SD card");
					}
				}
				System.out.println("cpoy finish");

			}
		}).start();
	}
}
