package com.chuangxu.tourguide.data;

import java.io.IOException;
import java.io.InputStream;

import org.gl.GlobalVariable;
import org.gl.utils.DataSources;
import org.gl.utils.IOUtil;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Administrator
 * 
 */
// {"content":"content....","id":"1","spots":[{"id":"1_0","content":"content....","icon":"icon.png","splash":"icon.png","audio":"nanrenhaonan","name":"LaoJieShan","map":{},"images":["1_0.png","1_1.png","1_2.png","1_3.png","1_4.png"]},{"id":"1_1","content":"content....","icon":"icon.png","splash":"icon.png","audio":"nanrenhaonan","name":"LaoJieShan","map":{},"images":["1_0.png","1_1.png","1_2.png","1_3.png","1_4.png"]},{"id":"1_2","content":"content....","icon":"icon.png","splash":"icon.png","audio":"nanrenhaonan","name":"LaoJieShan","map":{},"images":["1_0.png","1_1.png","1_2.png","1_3.png","1_4.png"]},{"id":"1_3","content":"content....","icon":"icon.png","splash":"icon.png","audio":"nanrenhaonan","name":"LaoJieShan","map":{},"images":["1_0.png","1_1.png","1_2.png","1_3.png","1_4.png"]},{"id":"1_4","content":"content....","icon":"icon.png","splash":"icon.png","audio":"nanrenhaonan","name":"LaoJieShan","map":{},"images":["1_0.png","1_1.png","1_2.png","1_3.png","1_4.png"]}],"icon":"icon.png","audio":"nanrenhaonan","splash":"icon.png","name":"LaoJieShan","map":{},"images":["1_0.png","1_1.png","1_2.png","1_3.png","1_4.png"]}
public class ScenicsDataSources extends DataSources {
	static JSONObject DATA_LaoJieLin = null;

	public static JSONObject getdata() {
		if (DATA_LaoJieLin == null) {
			InputStream is;
			try {
				is = GlobalVariable.applicationContext.getResources()
						.getAssets().open("laojieshan");
				DATA_LaoJieLin = new JSONObject(IOUtil.readString(is));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
		}
		return DATA_LaoJieLin;
	}
}
