package com.chuangxu.tourguide.audio;

import java.io.IOException;

import org.gl.utils.Dateutil;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chuangxu.tourguide.ui.R;

public class AudioPlayer implements View.OnClickListener {
	private static View buttonStart;

	private View buttonPause;

	private static View buttonStop;

	private ProgressBar progressBar;

	TextView TextView_time;
	String AssetName;

	public AudioPlayer(Context _context, MediaViewHandlerI mMediaViewHandlerI,
			String AssetName) throws IOException {
		initView(_context, mMediaViewHandlerI);
		resetPlayer();
		if (!TextUtils.isEmpty(AssetName)) {
			System.out.println("url:" + AssetName);
			this.AssetName = AssetName;
			mediaPlayer = MediaPlayer.create(context, Uri.parse(AssetName));
			if (mediaPlayer == null) {
				AssetFileDescriptor fileDescriptor = _context.getAssets()
						.openFd(AssetName);
				mediaPlayer = new MediaPlayer();
				mediaPlayer.setDataSource(fileDescriptor.getFileDescriptor(),
						fileDescriptor.getStartOffset(),
						fileDescriptor.getLength());
				mediaPlayer.prepare();
			}
			if (mediaPlayer != null) {
				setListener();
			} else {
				System.out.println("mediaPlayer fail");
				System.out.println("mediaPlayer fail");
				System.out.println("mediaPlayer fail");
			}
		}
	}

	private void resetPlayer() {
		if (mediaPlayer != null) {
			StopPlayer();
		}
		mainActivity = null;
	};

	public AudioPlayer(Context _context, MediaViewHandlerI mMediaViewHandlerI,
			int resourceId) {
		initView(_context, mMediaViewHandlerI);
		resetPlayer();
		if (resourceId != 0) {
			mediaPlayer = MediaPlayer.create(context, resourceId);
		}
		System.out.println("MediaPlayer.create");
		this.resourceId = resourceId;
		if (mediaPlayer != null) {
			setListener();
		}
	}

	private void initView(Context _context, MediaViewHandlerI mMediaViewHandlerI) {
		this.context = _context;
		this.buttonStart = mMediaViewHandlerI.getStartButton();
		if (buttonStart != null) {
			buttonStart.setOnClickListener(this);
		}
		this.buttonPause = mMediaViewHandlerI.getPauseButton();
		if (buttonPause != null) {
			buttonPause.setOnClickListener(this);
		}
		this.buttonStop = mMediaViewHandlerI.getStopButton();
		if (buttonStop != null) {
			buttonStop.setOnClickListener(this);
		} else {
			buttonStop = new Button(context);
		}
		this.progressBar = mMediaViewHandlerI.getProgressBar();
		progressBar.setMax(100);
		progressBar.setProgress(0);
		this.TextView_time = mMediaViewHandlerI.getTimeTextView();
	}

	private void setListener() {
		// mediaPlayer.prepareAsync();
		mediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {

			@Override
			public boolean onInfo(MediaPlayer mp, int what, int extra) {
				System.out.println("onInfo:" + mp.getCurrentPosition() + ","
						+ mp.getDuration());
				progressBar.setProgress(mp.getCurrentPosition());
				progressBar.setMax(mp.getDuration());
				return false;
			}
		});
		// mediaPlayer.setOnTimedTextListener(new OnTimedTextListener() {
		//
		// @Override
		// public void onTimedText(MediaPlayer mp, TimedText text) {
		// progressBar.setProgress(mp.getCurrentPosition());
		// progressBar.setMax(mp.getDuration());
		// System.out.println("timeText" + text.getText());
		//
		// }
		// });
		mediaPlayer.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				changeplayButtonState(STOPED);
				StopPlayer();
			}
		});
		mediaPlayer.setOnErrorListener(new OnErrorListener() {

			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				changeplayButtonState(STOPED);
				Toast.makeText(context, "err:" + what + "extra:" + extra,
						Toast.LENGTH_SHORT).show();
				StopPlayer();
				return false;
			}
		});
	};

	int resourceId;

	public void startPlayer() {
		if (mediaPlayer == null) {
			if (resourceId != 0) {
				mediaPlayer = MediaPlayer.create(context, resourceId);
			} else {
				AssetFileDescriptor fileDescriptor;
				try {
					fileDescriptor = context.getAssets().openFd(AssetName);
					mediaPlayer = new MediaPlayer();
					mediaPlayer.setDataSource(
							fileDescriptor.getFileDescriptor(),
							fileDescriptor.getStartOffset(),
							fileDescriptor.getLength());
					mediaPlayer.prepare();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}
		if (mediaPlayer.isPlaying()) {
			PausePlayer();
			return;
		}
		mediaPlayer.start();
		// ----------定时器记录播放进度---------//
		handler.post(mrenner);
		changeplayButtonState(PLAYING);
	};

	static Handler handler = new Handler();
	Runnable mrenner = new Runnable() {
		@Override
		public void run() {
			if (mediaPlayer != null && TextView_time != null
					&& progressBar != null) {
//				System.out.println("mTimerTask->current:total"
//						+ mediaPlayer.getCurrentPosition() + ","
//						+ mediaPlayer.getDuration());
				progressBar.setProgress(mediaPlayer.getCurrentPosition() * 100
						/ mediaPlayer.getDuration());
				TextView_time.setText(Dateutil
						.milliSecendToHMSString(mediaPlayer
								.getCurrentPosition()));
				// + ""
				// + Dateutil.milliSecendToHMSString(mediaPlayer
				// .getDuration())
				handler.postDelayed(this, 1000);
			}
		}
	};
	final static int PLAYING = 1;
	final static int PASUEING = 0;
	final static int STOPED = 2;

	private static void changeplayButtonState(int state) {
		switch (state) {
		case PLAYING:
			buttonStart.setBackgroundResource(R.drawable.ic_media_pause);
			buttonStop.setEnabled(true);
			break;
		case PASUEING:
			buttonStart.setBackgroundResource(R.drawable.ic_media_play);
			buttonStop.setEnabled(true);
			break;
		case STOPED:
			buttonStart.setBackgroundResource(R.drawable.ic_media_play);
			buttonStop.setEnabled(false);
			break;
		default:
			break;
		}

	}

	static MediaPlayer mediaPlayer = null;
	Context context;

	// 设置相应imageButton的onClickListener 进行

	@Override
	public void onClick(View v) {
		if (buttonStart == v) {
			startPlayer();
		} else if (buttonPause == v) {
			PausePlayer();
		} else if (buttonStop == v) {
			StopPlayer();
		}

	}

	public static void StopPlayer() {
		if (mediaPlayer != null) {
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.stop();
				changeplayButtonState(STOPED);
			}
			handler.removeCallbacksAndMessages(null);
			mediaPlayer.release();
			mediaPlayer = null;

		}
		if (mainActivity != null) {
			mainActivity.onPlayStop();
			mainActivity = null;
		}
		
	}

	private void PausePlayer() {
		if (mediaPlayer != null) {
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.pause();
				changeplayButtonState(PASUEING);
			}

		}

	}

	public interface OnPlayStopListener {
		public void onPlayStop();
	}

	static OnPlayStopListener mainActivity;

	public void setOnPlayStopListener(OnPlayStopListener mainActivity) {
		this.mainActivity = mainActivity;

	}

}
