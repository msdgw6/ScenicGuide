package com.chuangxu.tourguide.audio;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

public interface MediaViewHandlerI {
	public View getStartButton();
	public View getPauseButton();
	public View getStopButton();
	public ProgressBar getProgressBar();
	public TextView getTimeTextView();
}
