package com.chuangxu.tourguide.common.util;


import android.text.InputFilter;
import android.text.Spanned;

public class AdnNameLengthFilter implements InputFilter{
     public final static int LENGTH_CNAME = 12;
     public final static int LENGTH_ENAME = 24;
     public final static int LENGTH_MNAME = 18;
	private int nMax;
	@Override
	public CharSequence filter(CharSequence source, int start, int end,
			Spanned dest, int dstart, int dend) {
		// TODO Auto-generated method stub

         int num = getChineseNum(dest.toString());

        int sourceChineseNum = getChineseNum(source.toString());
    
    		  nMax = 24-num;
      

            int keep = nMax - (dest.length() - (dend - dstart));  

            if (keep <= 0) {  
                return "";  
            } else if (keep > end - start&&sourceChineseNum==0) {  
                return null; // keep original   
            } else if(keep>end-start&&sourceChineseNum!=0&&keep/2<end-start){
            	return source.subSequence(start, start+keep/2);
            }else if(keep>end-start&&sourceChineseNum!=0&&keep/2>end-start){
            	return null;
            }
            else if(keep>0&&keep<=end-start&&sourceChineseNum==0){  
                return source.subSequence(start, start + keep);
            	}  else{
            		return source.subSequence(start, start+keep/2);
            	}
	}

  public int getChineseNum(String string){
      int count = 0;
    int    length = string.length();
  
	for(int i=0;i<length;i++){
		if(string.substring(i,i+1).matches("[\\u4e00-\\u9fbf]+")){
			count++;
		} 
	}
	
	return count;
  }
}
