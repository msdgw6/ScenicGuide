package com.example.osmtest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import microsoft.mappoint.TileSystem3;
import microsoft.mappoint.TileSystem3.MapSystemInfo;
import net.tsz.afinal.FinalDb;

import org.json.JSONArray;
import org.json.JSONException;
import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.events.MapListener;
import org.osmdroid.events.ScrollEvent;
import org.osmdroid.events.ZoomEvent;
import org.osmdroid.tileprovider.constants.OpenStreetMapTileProviderConstants;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.MapView.Projection;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.Overlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chuangxu.tourguide.ActivityExtraDataInterface;
import com.chuangxu.tourguide.audio.AudioPlayer;
import com.chuangxu.tourguide.audio.AudioPlayer.OnPlayStopListener;
import com.chuangxu.tourguide.audio.MediaViewHandlerI;
import com.chuangxu.tourguide.resource.InitResourcer;
import com.chuangxu.tourguide.ui.R;
import com.chuangxu.tourguide.ui.SpotActivity;
import com.nostra13.example.universalimageloader.BaseActivity;

public class MainActivity extends BaseActivity implements MediaViewHandlerI,
		OnPlayStopListener {
	private MapController mapController;
	private MapView mapView;
	private TextView mTxtCenter;
	private Button mBtnGetter;
	private List<Overlay> mListOfOverlays;
	private CustomMapOverlay lineOverlay = null;
	private MyLocationNewOverlay myLocationOverlay = null;
	private OsmMapsItemizedOverlay mItemizedOverlay = null;
	private ArrayList<OverlayItem> mItemList = new ArrayList<OverlayItem>();
	private DefaultResourceProxyImpl mResourceProxy;
	private View mPopuView;
	private mapInfo mMapInfo;
	private TextView map_bubbleTitle;
	private List<SpotInfo> mListSpotInfo;
	private Map<Integer, List<SpotInfo>> mSpotMap = new HashMap<Integer, List<SpotInfo>>();
	private Map<Integer, List<OverlayItem>> mItemMap = new HashMap<Integer, List<OverlayItem>>();
	private View map_share;
	private View map_toGPS;
	private long mBeginTime = 0;
	public final long HIDE_TIME = 2000;
	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 10002:
				if (mPopuView.getVisibility() == View.VISIBLE
						|| new Date().getTime() - HIDE_TIME < mBeginTime) {
					return;
				}
				for (int i = 0; i <= mapView.getZoomLevel(); i++) {
					if (mItemMap.containsKey(i)) {
						ArrayList<OverlayItem> sls = (ArrayList<OverlayItem>) mItemMap
								.get(i);
						// 方法3
						for (OverlayItem tmp : sls) {
							mItemizedOverlay.removeAllItems();
						}
					}
				}
				mapView.invalidate();
				break;
			default:
				break;
			}
		}
	};
	public void onAttachedToWindow() {
		// TODO Auto-generated method stub
		super.onAttachedToWindow();
		System.out.println("manin onAttachedToWindow");
		stopAudioPlayer();
	}
	private void addLevelInfo(int level, String strLevelData) {
		String[] strSplit1 = strLevelData.split(",");
		MapSystemInfo info = new MapSystemInfo();
		info.x = Integer.parseInt(strSplit1[0]);
		info.y = Integer.parseInt(strSplit1[1]);
		info.z = Double.parseDouble(strSplit1[2]);
		TileSystem3.setMapSystem(level, info);
	}

	private AudioPlayer m;

	@Override
	protected void onStop() {
		super.onStop();
		stopAudioPlayer();
		System.out.println("onstop");
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		stopAudioPlayer();
		System.out.println("onPause");
	}

	private void stopAudioPlayer() {
		if (m != null) {
			m.StopPlayer();
			m = null;
		}
	}

	private void pauseAudioPlayer() {
		if (m != null) {
			m.startPlayer();
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mTxtCenter = (TextView) this.findViewById(R.id.txtCenter);
		findViewById(R.id.ll_audioplay).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						findViewById(R.id.ll_audioplay)
								.setVisibility(View.GONE);
						pauseAudioPlayer();
					}
				});

		ImageButton btnMe = (ImageButton) this.findViewById(R.id.iv_user_edit);
		btnMe.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (myLocationOverlay.getMyLocation() != null)
					mapController.setCenter(myLocationOverlay.getMyLocation());
				else
					Toast.makeText(MainActivity.this, "还没有定位成功",
							Toast.LENGTH_SHORT).show();
			}

		});

		btnMe = (ImageButton) this.findViewById(R.id.project_update_button);
		btnMe.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, CarActivity.class);
				startActivity(intent);

			}

		});

		this.mBtnGetter = (Button) this.findViewById(R.id.btnGetCenter);
		this.mBtnGetter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// mTxtCenter.setText(String.format("%f,%f",mapView.getMapCenter().getLongitudeE6()/1E6,
				// mapView.getMapCenter().getLatitudeE6()/1E6));
				// GeoPoint geopoint = new GeoPoint(0.08, 0.05);
				// mapController.setCenter(geopoint);
			}

		});
		// 文件地址 FIXME warn OSMDROID中的加载数据库的地址为
		// Environment.getExternalStorageDirectory()+rnbs
		FinalDb db = FinalDb.create(this,
				OpenStreetMapTileProviderConstants.OSMDROID_PATH + "/"
						+ InitResourcer.map_db_name);
		List<mapInfo> ls = db.findAll(mapInfo.class);

		if (ls == null || ls.size() == 0) {
			return;
		}
		mMapInfo = ls.get(0);

		mListSpotInfo = db.findAllByWhere(SpotInfo.class, "regionid = '"
				+ mMapInfo.getRegionid() + "'");

		// 初始化坐标系统
		String[] strSplit1 = mMapInfo.getLefttopgps().split(",");
		String[] strSplit2 = mMapInfo.getRightbottomgps().split(",");

		double leftLongitude = Double.parseDouble(strSplit1[0]);
		double leftLatitude = Double.parseDouble(strSplit1[1]);

		TileSystem3.setMapBound(Double.parseDouble(strSplit2[1]), leftLatitude,
				leftLongitude, Double.parseDouble(strSplit2[0]),
				mMapInfo.getMaxlevel());

		addLevelInfo(0, mMapInfo.getLevel0());

		if (mMapInfo.getMaxlevel() > 0)
			addLevelInfo(1, mMapInfo.getLevel1());
		if (mMapInfo.getMaxlevel() > 1)
			addLevelInfo(2, mMapInfo.getLevel2());
		if (mMapInfo.getMaxlevel() > 2)
			addLevelInfo(3, mMapInfo.getLevel3());

		mapView = (MapView) findViewById(R.id.map);

		mapView.setBackgroundColor(Color.WHITE);
		// http://mt3.google.cn/vt/v=w2.97&hl=zh-CN&gl=CN
		// 初始化系统瓦片数据源
		mapView.setTileSource(new GoogleMapsTileSource(
				"Google Maps6",
				null,
				0,
				mMapInfo.getMaxlevel(),
				256,
				".png",
				new String[] { "http://192.168.0.182:8011/srvapp/mapxyz/getmap?f=m" }));
		mapView.setUseDataConnection(false);

		// 设置系统经纬度边界
		BoundingBoxE6 boundingBox = new BoundingBoxE6(0,
				TileSystem3.MaxLongitude, TileSystem3.MaxLatitude, 0);

		mapView.setScrollableAreaLimit(boundingBox);

		// mapView.setBuiltInZoomControls(true);
		mapView.setMultiTouchControls(true);
		mapView.setClickable(false);
		mapController = mapView.getController();
		mapController.setZoom(0);

		mListOfOverlays = mapView.getOverlays();

		// 处理锚点 poi点击事件
		mResourceProxy = new DefaultResourceProxyImpl(this);
		PrepareItemizedOverlay();

		// 添加锚点 弹出窗口
		this.mPopuView = getLayoutInflater().inflate(R.layout.map_popup, null);

		map_bubbleTitle = (TextView) this.mPopuView
				.findViewById(R.id.map_bubbleTitle);
		map_share = (View) this.mPopuView.findViewById(R.id.map_share);
		map_share.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (SelectOverLayItem != null) {
					Intent intent = new Intent(MainActivity.this,
							SpotActivity.class);
					String id = SelectOverLayItem.getSnippet();
					boolean isexist = false;// 验证资源中是否存在这个景点ID
					if (dataStruct != null) {
						try {
							JSONArray spots = dataStruct.optJSONArray("spots");
							for (int i = 0; i < spots.length(); i++) {
								if (!TextUtils.isEmpty(id)
										&& id.equals(spots.getJSONObject(i)
												.optString("id"))) {
									isexist = true;
									break;
								}
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
					if (isexist) {
						intent.putExtra(ActivityExtraDataInterface.SPOTID, id);
						startActivity(intent);
					} else {
						Toast.makeText(
								getBaseContext(),
								"Spot:" + SelectOverLayItem.getTitle() + " id:"
										+ id + " is not exists",
								Toast.LENGTH_SHORT).show();
					}

				}

			}
		});
		// 播放语音
		map_toGPS = (View) this.mPopuView.findViewById(R.id.map_toGPS);
		map_toGPS.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (SelectOverLayItem != null) {
					Intent intent = new Intent(MainActivity.this,
							SpotActivity.class);
					String id = SelectOverLayItem.getSnippet();
					boolean isexist = false;// 验证资源中是否存在这个景点ID
					String audioName = null;
					String Name = null;
					if (dataStruct != null) {
						try {
							JSONArray spots = dataStruct.optJSONArray("spots");
							for (int i = 0; i < spots.length(); i++) {
								if (!TextUtils.isEmpty(id)
										&& id.equals(spots.getJSONObject(i)
												.optString("id"))) {
									audioName = spots.getJSONObject(i)
											.optString("audio");
									Name = spots.getJSONObject(i).optString(
											"name");
									isexist = true;
									break;
								}
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
					if (isexist && !TextUtils.isEmpty(audioName)) {
						try {
							if (Name.equals(((TextView) findViewById(R.id.name))
									.getText())
									&& m != null) {
								m.startPlayer();

							} else {
								m = new AudioPlayer(MainActivity.this,
										MainActivity.this, audioName);
								m.startPlayer();
								((TextView) findViewById(R.id.name))
										.setText(Name);
								m.setOnPlayStopListener(MainActivity.this);// FIXME
							}

							findViewById(R.id.ll_audioplay).setVisibility(
									View.VISIBLE);

						} catch (IOException e) {
							e.printStackTrace();
						}
					} else {
						findViewById(R.id.ll_audioplay)
								.setVisibility(View.GONE);
						Toast.makeText(
								getBaseContext(),
								"Spot:" + SelectOverLayItem.getTitle() + " id:"
										+ id + " is not exists",
								Toast.LENGTH_SHORT).show();
					}

				}

			}
		});
		GeoPoint geopoint = new GeoPoint(0, 0);
		MapView.LayoutParams mapParams = new MapView.LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.WRAP_CONTENT, geopoint,
				MapView.LayoutParams.BOTTOM_CENTER, 0, 0);

		mapView.addView(this.mPopuView, mapParams);
		mPopuView.setVisibility(8);

		MapOverlay mmapOverlay = new MapOverlay(this);

		mListOfOverlays.add(mmapOverlay);

		// 开启定位跟踪
		GpsMyLocationProvider m = new GpsMyLocationProvider(this);
		myLocationOverlay = new MyLocationNewOverlay(getApplicationContext(),
				m, mapView);
		mListOfOverlays.add(myLocationOverlay);
		myLocationOverlay.enableMyLocation(m);

		// 追加锚点 poi

		for (int i = 0; i < mListSpotInfo.size(); i++) {
			OverlayItem overlayItem;
			SpotInfo info = mListSpotInfo.get(i);

			String[] temp = info.getGps().split(",");

			double Longitude = Double.parseDouble(temp[0]);
			double Latitude = Double.parseDouble(temp[1]);

			GeoPoint point2 = new GeoPoint(leftLatitude - Latitude, Longitude
					- leftLongitude);
			// 绑定描点和图层的数据
			overlayItem = new OverlayItem(info.getName() + "", info.getId()
					+ "", (GeoPoint) point2);

			// if (info.getMinlevel() == 0)
			// mItemizedOverlay.addItem(overlayItem);

			// if (info.getMinlevel()) {
			if (mSpotMap.containsKey(info.getMinlevel())) {
				mSpotMap.get(info.getMinlevel()).add(info);
			} else {
				ArrayList<SpotInfo> sls = new ArrayList<SpotInfo>();
				sls.add(info);
				mSpotMap.put(info.getMinlevel(), sls);
			}

			if (mItemMap.containsKey(info.getMinlevel())) {
				mItemMap.get(info.getMinlevel()).add(overlayItem);
			} else {
				ArrayList<OverlayItem> sls = new ArrayList<OverlayItem>();
				sls.add(overlayItem);
				mItemMap.put(info.getMinlevel(), sls);
			}
			// }
		}

		mListOfOverlays.add(mItemizedOverlay);

		// MinimapOverlay minmap=new
		// MinimapOverlay(this,mapView.getHandler());
		// mapView.getOverlays().add(minmap);

		mapView.invalidate();

		mapView.setMapListener(new MapListener() {

			@Override
			public boolean onScroll(ScrollEvent event) {
				// mTxtCenter.setText(String.format("%d,%d",
				// mapView.getMapCenter().getLatitudeE6(),mapView.getMapCenter().getLongitudeE6()));
				/*
				 * mTxtCenter.setText(String.format("%f,%f", mapView
				 * .getMapCenter().getLongitudeE6() / 1E6, mapView
				 * .getMapCenter().getLatitudeE6() / 1E6));
				 */

				return false;
			}

			@Override
			public boolean onZoom(ZoomEvent event) {
				mPopuView.setVisibility(View.GONE);
				mItemizedOverlay.removeAllItems();
				mapView.invalidate();

				// event.getZoomLevel()
				/*
				 * if (event.getZoomLevel() <= mMapInfo.getMaxlevel()) {
				 * 
				 * for (int i = event.getZoomLevel() + 1; i <= mMapInfo
				 * .getMaxlevel(); i++) { if (mItemMap.containsKey(i)) {
				 * ArrayList<OverlayItem> sls = (ArrayList<OverlayItem>)
				 * mItemMap .get(i); // 方法3 for (OverlayItem tmp : sls) {
				 * mItemizedOverlay.removeItem(tmp); } } }
				 * 
				 * for (int i = 1; i <= event.getZoomLevel(); i++) { if
				 * (mItemMap.containsKey(i)) { ArrayList<OverlayItem> sls =
				 * (ArrayList<OverlayItem>) mItemMap .get(i); // 方法3 for
				 * (OverlayItem tmp : sls) { mItemizedOverlay.addItem(tmp); } }
				 * }
				 * 
				 * } mHandler.postDelayed(new Runnable() { public void run() {
				 * mHandler.sendEmptyMessage(10002); } }, HIDE_TIME);
				 * mapView.invalidate();
				 */

				return false;
			}

			@Override
			public boolean onDown(MotionEvent e) {
				for (int i = 0; i <= mapView.getZoomLevel(false); i++) {
					if (mItemMap.containsKey(i)) {
						ArrayList<OverlayItem> sls = (ArrayList<OverlayItem>) mItemMap
								.get(i);
						// 方法3
						for (OverlayItem tmp : sls) {
							mItemizedOverlay.addItem(tmp);
						}
					}
				}
				mBeginTime = new Date().getTime();
				mHandler.postDelayed(new Runnable() {
					public void run() {
						mHandler.sendEmptyMessage(10002);
					}
				}, 2000);

				mapView.invalidate();

				return false;
			}

		});
	}

	private OverlayItem SelectOverLayItem = null;

	private void PrepareItemizedOverlay() {

		// itemized overlay
		mItemizedOverlay = new OsmMapsItemizedOverlay(this.mItemList,
				new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {

					@Override
					public void onClick() {
						mPopuView.setVisibility(View.GONE);
					}

					@Override
					public boolean onItemSingleTapUp(final int index,
							final OverlayItem item) {
						OverlayItem localOverlayItem = item;
						// setFocus(localOverlayItem);
						MapView.LayoutParams localLayoutParams = (MapView.LayoutParams) mPopuView
								.getLayoutParams();

						// localLayoutParams.offsetX = (- mPopuView.getWidth() /
						// 2);
						// localLayoutParams.offsetY = (-mPopuView.getHeight());
						localLayoutParams.geoPoint = localOverlayItem
								.getPoint();
						mapView.updateViewLayout(mPopuView, localLayoutParams);
						mPopuView.setVisibility(0);
						mapController.animateTo(localOverlayItem.getPoint());
						map_bubbleTitle.setText(item.getTitle());
						SelectOverLayItem = item;
						// Toast.makeText(getBaseContext(),
						// item.getPoint().toDoubleString(),
						// Toast.LENGTH_SHORT).show();
						/*
						 * Toast.makeText( MainActivity.this, "Item  (index=" +
						 * index + ") got single tapped up",
						 * Toast.LENGTH_LONG).show();
						 */

						return true; // We 'handled' this event.
					}

					@Override
					public boolean onItemLongPress(final int index,
							final OverlayItem item) {

						Toast.makeText(MainActivity.this,
								"Item  (index=" + index + ") got long pressed",
								Toast.LENGTH_LONG).show();

						return true;
					}
				}, mResourceProxy);
	}

	public class MapOverlay extends org.osmdroid.views.overlay.Overlay {

		public MapOverlay(Context ctx) {
			super(ctx);
		}

		private int mVpl;// viewport left, top, right, bottom
		private int mVpt;
		private int mVpr;
		private int mVpb;
		private MapView mMv = null;

		// Two routines to transform and scale between viewport and mapview
		private float transformX(float in, MapView mv) {
			float out;
			out = ((mVpr - mVpl) * in) / (mv.getRight() - mv.getLeft()) + mVpl;
			return out;
		}

		private float transformY(float in, MapView mv) {
			float out;
			out = ((mVpb - mVpt) * in) / (mv.getBottom() - mv.getTop()) + mVpt;
			return out;
		}

		@Override
		protected void draw(Canvas pC, MapView mapV, boolean shadow) {
			if (shadow)
				return;

			Paint paint;
			paint = new Paint();
			paint.setColor(Color.RED);
			paint.setAntiAlias(true);
			paint.setStyle(Style.STROKE);
			paint.setStrokeWidth(1);
			paint.setTextAlign(Paint.Align.LEFT);
			paint.setTextSize(12);
			final Rect viewportRect = new Rect();
			final Projection projection = mapV.getProjection();
			viewportRect.set(projection.getScreenRect());
			mVpl = viewportRect.left;
			mVpt = viewportRect.top;
			mVpr = viewportRect.right;
			mVpb = viewportRect.bottom;
			// draw two lines to split screen into 2x2 quarters
			// drag the map left and right and the vertical line disappears,
			// then reappears! It's OK at one less zoom level
			// pC.drawLine(transformX(mapV.getWidth() / 2, mapV),
			// transformY(0, mapV), transformX(mapV.getWidth() / 2, mapV),
			// transformY(mapV.getHeight(), mapV), paint);
			//
			// pC.drawLine(transformX(0, mapV),
			// transformY(mapV.getHeight() / 2, mapV),
			// transformX(mapV.getWidth(), mapV),
			// transformY(mapV.getHeight() / 2, mapV), paint);
		}
	}

	@Override
	public View getStartButton() {
		return findViewById(R.id.imageButton1);
	}

	@Override
	public View getPauseButton() {
		return findViewById(R.id.imageButton1);
	}

	@Override
	public View getStopButton() {
		return null;
	}

	@Override
	public ProgressBar getProgressBar() {
		return (ProgressBar) findViewById(R.id.progressBar1);
	}

	@Override
	public TextView getTimeTextView() {
		return (TextView) findViewById(R.id.textView3);
	}

	@Override
	public void onPlayStop() {
		findViewById(R.id.ll_audioplay).setVisibility(View.GONE);

	}

}
