package com.example.osmtest;

import net.tsz.afinal.annotation.sqlite.Table;

@Table(name = "mapinfo")
public class mapInfo {
	private int id;
	private int maxlevel;
	private int regionid;

	private String lefttopgps;
	private String rightbottomgps;

	private String level0;
	private String level1;
	private String level2;
	private String level3;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMaxlevel() {
		return maxlevel;
	}

	public void setMaxlevel(int maxlevel) {
		this.maxlevel = maxlevel;
	}

	public String getLevel0() {
		return level0;
	}

	public void setLevel0(String level0) {
		this.level0 = level0;
	}

	public int getRegionid() {
		return regionid;
	}

	public void setRegionid(int regionid) {
		this.regionid = regionid;
	}

	public String getLefttopgps() {
		return lefttopgps;
	}

	public void setLefttopgps(String lefttopgps) {
		this.lefttopgps = lefttopgps;
	}

	public String getRightbottomgps() {
		return rightbottomgps;
	}

	public void setRightbottomgps(String rightbottomgps) {
		this.rightbottomgps = rightbottomgps;
	}

	public String getLevel1() {
		return level1;
	}

	public void setLevel1(String level1) {
		this.level1 = level1;
	}

	public String getLevel2() {
		return level2;
	}

	public void setLevel2(String level2) {
		this.level2 = level2;
	}

	public String getLevel3() {
		return level3;
	}

	public void setLevel3(String level3) {
		this.level3 = level3;
	}

}
