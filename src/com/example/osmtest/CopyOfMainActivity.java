//package com.example.osmtest;
//
//import java.util.ArrayList;
//import java.util.List;
//
// 
//
//import microsoft.mappoint.TileSystem3;
//import microsoft.mappoint.TileSystem3.MapSystemInfo;
//import net.tsz.afinal.FinalDb;
//
//import org.osmdroid.DefaultResourceProxyImpl;
//import org.osmdroid.events.MapListener;
//import org.osmdroid.events.ScrollEvent;
//import org.osmdroid.events.ZoomEvent;
//import org.osmdroid.util.BoundingBoxE6;
//import org.osmdroid.util.GeoPoint;
//import org.osmdroid.views.MapController;
//import org.osmdroid.views.MapView;
//import org.osmdroid.views.MapView.Projection;
//import org.osmdroid.views.overlay.ItemizedIconOverlay;
//import org.osmdroid.views.overlay.Overlay;
//import org.osmdroid.views.overlay.OverlayItem;
//import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
//import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;
// 
//import android.os.Bundle;
//import android.app.Activity;
//import android.content.Context;
//import android.graphics.Canvas;
//import android.graphics.Color;
//import android.graphics.Paint;
//import android.graphics.Paint.Style;
//import android.graphics.Rect;
//import android.util.Log;
//import android.view.Menu;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.View.OnTouchListener;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.TextView;
//import android.widget.Toast;
//
//public class CopyOfMainActivity extends Activity {
//	private MapController mapController;
//    private MapView mapView;
//    private TextView mTxtCenter;
//    private Button mBtnGetter;
//    private List<Overlay> mListOfOverlays;
//    private CustomMapOverlay lineOverlay = null;
//    private MyLocationNewOverlay myLocationOverlay = null;
//    private  OsmMapsItemizedOverlay mItemizedOverlay = null;
//    private ArrayList<OverlayItem> mItemList = new ArrayList<OverlayItem>();
//    private DefaultResourceProxyImpl mResourceProxy;
//    private View mPopuView;
//    private mapInfo mMapInfo;
//    
//    private void addLevelInfo(int level,String strLevelData) {
//    	String[] strSplit1 = strLevelData.split(",");
//    	MapSystemInfo info = new MapSystemInfo();
//    	info.x = Integer.parseInt(strSplit1[0]);
//    	info.y = Integer.parseInt(strSplit1[1]);
//    	info.z = Integer.parseInt(strSplit1[2]);
//    	TileSystem3.setMapSystem(level,info);
//    }
//    
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_main);
//		
//		FinalDb db = FinalDb.create(this, "//sdcard//osmdroid//map.mbtiles");
//        
//        List<mapInfo> ls = db.findAll(mapInfo.class);
//        
//        if(ls.size() > 0)  {
//        	mMapInfo = ls.get(0);
//        	
//        	//double minLat,double maxLat, double minLong, double maxLong,int maxLevel
//        	String[] strSplit1 = mMapInfo.getLefttopgps().split(",");
//        	String[] strSplit2 = mMapInfo.getRightbottomgps().split(",");
//        	
//        	TileSystem3.setMapBound(Double.parseDouble(strSplit2[1]),
//        			Double.parseDouble(strSplit1[1]),
//        					Double.parseDouble(strSplit1[0]),
//        					Double.parseDouble(strSplit2[0]),
//        					mMapInfo.getMaxlevel()
//        					);
//        	
//        	
//        	addLevelInfo(0,mMapInfo.getLevel0());
//        	
//        	if(mMapInfo.getMaxlevel() > 0)
//        		addLevelInfo(1,mMapInfo.getLevel1());
//        	if(mMapInfo.getMaxlevel() > 1)
//        		addLevelInfo(2,mMapInfo.getLevel2());
//        	if(mMapInfo.getMaxlevel() > 2)
//        		addLevelInfo(3,mMapInfo.getLevel3());
//        	
//		
//		this.mPopuView = getLayoutInflater().inflate(R.layout.map_popup, null);
//		
//		this.mBtnGetter = (Button) this.findViewById(R.id.btnGetCenter);
//		this.mBtnGetter.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				//mTxtCenter.setText(String.format("%f,%f",mapView.getMapCenter().getLongitudeE6()/1E6, mapView.getMapCenter().getLatitudeE6()/1E6));
//				 GeoPoint geopoint=new GeoPoint(0.08,0.05);
//			        mapController.setCenter(geopoint);  
//			}
//			
//		});
//		
//		mTxtCenter = (TextView) this.findViewById(R.id.txtCenter);
//		
//		mapView = (MapView) findViewById(R.id.map);  
//		//http://mt3.google.cn/vt/v=w2.97&hl=zh-CN&gl=CN
//        mapView.setTileSource(new GoogleMapsTileSource("Google Maps6", null, 0, mMapInfo.getMaxlevel(), 256, ".png", 
//       		new String[]{"http://192.168.0.182:8011/srvapp/mapxyz/getmap?f=m"}));  
//        
//       // mapView.setTileSource(new GoogleMapsTileSource("Google Maps3", null, 1, 20, 256, ".png", 
//       // 		new String[]{"http://mt3.google.cn/vt/v=w2.97&hl=zh-CN&gl=CN"})); 
//        
//        //"http://mt3.google.com/vt/v=w2.97&hl=zh-CN"
//		//mapView.setTileSource(new GoogleMapsTileSource("Google Maps", null, 1, 20, 256, ".png", new String[]{"http://shangetu3.map.bdimg.com/it/u="}));  
//        
//        
//        
//        
//		mapView.setUseDataConnection(false);
//        
// 
//        
//        BoundingBoxE6 boundingBox = new BoundingBoxE6(0,TileSystem3.MaxLongitude,TileSystem3.MaxLatitude,0  );
//        
//        mapView.setScrollableAreaLimit(boundingBox);
//        
//        mapView.setBuiltInZoomControls(true);  
//        mapView.setMultiTouchControls(true);
//        mapView.setClickable(false);
//        mapController = mapView.getController();  
//        mapController.setZoom(0);  
//        //33.62323611111111, 111.771275
//       // GeoPoint geopoint=new GeoPoint(33.623768001188125,111.77078247070312);   
//       // GeoPoint geopoint=new GeoPoint(30.6405006,113.1222002);
//        GeoPoint geopoint=new GeoPoint(0,0);
//      //  mapController.setCenter(geopoint);  
//        
//        mListOfOverlays = mapView.getOverlays();
//        
//        MapView.LayoutParams mapParams = new MapView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 
//                ViewGroup.LayoutParams.WRAP_CONTENT,
//                geopoint,
//                MapView.LayoutParams.BOTTOM_CENTER, 0, 0);
//        
//        mapView.addView(this.mPopuView, mapParams);
//       //  mapView.addView(this.mPopuView, new MapView.LayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)));
//		 mPopuView.setVisibility(8);
//         
//        MapOverlay mmapOverlay = new MapOverlay(this);
//        
//        mListOfOverlays.add(mmapOverlay);
//        
//      //����һ���ߵ�ͼ��  
//        this.lineOverlay = new CustomMapOverlay(this);           
//              
//        mListOfOverlays.add(lineOverlay); 
//        
//        GpsMyLocationProvider m = new GpsMyLocationProvider(this);
//        myLocationOverlay = new MyLocationNewOverlay(getApplicationContext(),m, mapView);  
//        mListOfOverlays.add(myLocationOverlay);  
//        myLocationOverlay.enableMyLocation(m);
//        
//        
//        
//        OverlayItem overlayItem;
//        GeoPoint point2 = new GeoPoint(0.08,0.05);
//        overlayItem = new OverlayItem("Center", "Center", (GeoPoint) point2);
//
//        mResourceProxy = new DefaultResourceProxyImpl(this);
//
//        PrepareItemizedOverlay();
//
//        mItemizedOverlay.addItem(overlayItem);
//        
//        point2 = new GeoPoint(0.09,0.05);
//        overlayItem = new OverlayItem("Center", "Center", (GeoPoint) point2);
//        mItemizedOverlay.addItem(overlayItem);
//        
//        
//        mListOfOverlays.add(mItemizedOverlay);
//        
//       // MinimapOverlay minmap=new MinimapOverlay(this,mapView.getHandler());
//       // mapView.getOverlays().add(minmap);
//		
//        mapView.invalidate();
//        
//        mapView.setOnTouchListener(new OnTouchListener() {
//
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				// TODO Auto-generated method stub
//				Log.d("onTouch", "onTouch");
//				return false;
//			}
//        	
//        });
//        
//        mapView.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Log.d("onClick", "onClick");
//			}
//        	
//        });
//        
//        mapView.setMapListener(new MapListener() {
//
//			@Override
//			public boolean onScroll(ScrollEvent event) {
//				// TODO Auto-generated method stub
//				//mTxtCenter.setText(String.format("%d,%d", mapView.getMapCenter().getLatitudeE6(),mapView.getMapCenter().getLongitudeE6()));
//				mTxtCenter.setText(String.format("%f,%f", mapView.getMapCenter().getLongitudeE6()/1E6,mapView.getMapCenter().getLatitudeE6()/1E6));
//				
//				return false;
//			}
//
//			@Override
//			public boolean onZoom(ZoomEvent event) {
//				// TODO Auto-generated method stub
//				
//				return false;
//			}
//        	
//        });
//        }
//	}
//
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		return true;
//	}
//
//	
//	public class MapOverlay extends org.osmdroid.views.overlay.Overlay {
//
//        public MapOverlay(Context ctx) {super(ctx); }
//
//        private int mVpl;// viewport left, top, right, bottom
//        private int mVpt;
//        private int mVpr;
//        private int mVpb;
//        private MapView mMv = null;
//
//        // Two routines to transform and scale between viewport and mapview
//        private float transformX(float in, MapView mv) {
//            float out;
//            out = ((mVpr - mVpl) * in) / (mv.getRight() - mv.getLeft())
//                    + mVpl;
//            return out;
//        }
//
//        private float transformY(float in, MapView mv) {
//            float out;
//            out = ((mVpb - mVpt) * in) / (mv.getBottom() - mv.getTop())
//                    + mVpt;
//            return out;
//        }
//
//        @Override
//        protected void draw(Canvas pC, MapView mapV, boolean shadow) {
//            if (shadow)
//                return;
//
//            Paint paint;
//            paint = new Paint();
//            paint.setColor(Color.RED);
//            paint.setAntiAlias(true);
//            paint.setStyle(Style.STROKE);
//            paint.setStrokeWidth(1);
//            paint.setTextAlign(Paint.Align.LEFT);
//            paint.setTextSize(12);
//            final Rect viewportRect = new Rect();
//            final Projection projection = mapV.getProjection();
//            viewportRect.set(projection.getScreenRect());
//            mVpl = viewportRect.left;
//            mVpt = viewportRect.top;
//            mVpr = viewportRect.right;
//            mVpb = viewportRect.bottom;
//            // draw two lines to split screen into 2x2 quarters
//            // drag the map left and right and the vertical line disappears,
//            // then reappears! It's OK at one less zoom level
//            pC.drawLine(transformX(mapV.getWidth()/2, mapV), transformY(0, mapV),
//                    transformX(mapV.getWidth()/2, mapV),
//                    transformY(mapV.getHeight(), mapV), paint);
//
//            pC.drawLine(transformX(0, mapV), transformY(mapV.getHeight()/2, mapV),
//                    transformX(mapV.getWidth(), mapV),
//                    transformY(mapV.getHeight()/2, mapV), paint);
//        }
//    }
//	
//	private void PrepareItemizedOverlay()
//    {
//        
//		// itemized overlay 
//        mItemizedOverlay = new OsmMapsItemizedOverlay(this.mItemList,
//                new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>()
//                {
//        	@Override
//        	public void onClick(){
//        		mPopuView.setVisibility(View.GONE);
//		        	}
//                    @Override
//                    public boolean onItemSingleTapUp(final int index, final OverlayItem item)
//                    {
//                    	OverlayItem localOverlayItem = item;
//                       // setFocus(localOverlayItem);
//                        MapView.LayoutParams localLayoutParams = (MapView.LayoutParams)mPopuView.getLayoutParams();
//                        
//                       // localLayoutParams.offsetX = (- mPopuView.getWidth() / 2);
//                       // localLayoutParams.offsetY = (-mPopuView.getHeight());
//                        localLayoutParams.geoPoint = localOverlayItem.getPoint();
//                        mapView.updateViewLayout(mPopuView, localLayoutParams);
//                        mPopuView.setVisibility(0);
//                        mapController.animateTo(localOverlayItem.getPoint());
//                        /*        this.title.setText(localOverlayItem.getTitle());
//                        this.snippet.setText(this.address);
//                        this.snippet.setText(R.string.map_load_point);
//                        this.positionInfo.EmptyData();
//                        this.positionInfo.title = localOverlayItem.getTitle();
//                        this.positionInfo.point = localLayoutParams.point;
//                     */
//                        
//                        Toast.makeText(CopyOfMainActivity.this,
//                                "Item  (index=" + index + ") got single tapped up",
//                                Toast.LENGTH_LONG).show();
//
//                        return true; // We 'handled' this event.
//                    }
//
//                    @Override
//                    public boolean onItemLongPress(final int index, final OverlayItem item)
//                    {
//
//                        Toast.makeText(CopyOfMainActivity.this,
//                                "Item  (index=" + index + ") got long pressed", Toast.LENGTH_LONG)
//                                .show();
//
//                        return true;
//                    }
//                }, mResourceProxy);
//    }
//}
