package com.example.osmtest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mapapi.map.ItemizedOverlay;
import com.baidu.mapapi.map.MapController;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.OverlayItem;
import com.baidu.mapapi.map.PopupClickListener;
import com.baidu.mapapi.map.PopupOverlay;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.chuangxu.tourguide.ui.R;

public class CarItemizedOverlay extends ItemizedOverlay<OverlayItem> implements
		View.OnClickListener {
	private String address;

	private Context mContext;
	private MapController mController;
	private String mFlag;
	private MapView mMapView;
	private Drawable mMark;
	private View mPopuView;
	private View mPopuLeft;
	private View mPopuRight;

	private PopupOverlay pop = null;

	private GeoPoint mPoint;

	private Bitmap mBitmapLeft;
	private Bitmap mBitmapRight;

	private Handler m_handler = new Handler() {
		public void handleMessage(Message paramMessage) {
			switch (paramMessage.what) {
			default:
				break;
			case 1007:
				CarItemizedOverlay.this.address = ((String) paramMessage.obj);
				CarItemizedOverlay.this.snippet
						.setText(CarItemizedOverlay.this.address);
			//	CarItemizedOverlay.this.positionInfo.address = CarItemizedOverlay.this.address;

				Bitmap popbitmap = convertViewToBitmap(mPopuView);
				mBitmapLeft = convertViewToBitmap(mPopuLeft);
				mBitmapRight = convertViewToBitmap(mPopuRight);

				Bitmap[] bmps = new Bitmap[3];

				bmps[0] = mBitmapLeft;// BitmapFactory.decodeStream(mContext.getAssets().open("pop_left.png"));
				bmps[1] = popbitmap;//popbitmap;// BitmapFactory.decodeStream(mContext.getAssets().open("marker2.png"));
				bmps[2] = mBitmapRight;// BitmapFactory.decodeStream(mContext.getAssets().open(
				// "pop_right.png"));

				pop.showPopup(bmps, mPoint, 32);

				break;
			case 0:
				Log.d("CarItemizedOverlay",
						"====================net prompt=====================");
				break;
			}

		}
	};
	private SpotInfo positionInfo;
	private TextView snippet;
	private TextView title;

	Toast mToast = null;

	public CarItemizedOverlay(Context paramCarActivity, Drawable paramDrawable,
			View popCenterView, View popLeftView, View popRightView,
			MapView paramMapView, MapController paramMapController,
			String paramString) {
		super(paramDrawable, paramMapView);
		this.mContext = paramCarActivity;
		this.mMark = paramDrawable;
		this.mPopuView = popCenterView;
		this.mMapView = paramMapView;
		this.mController = paramMapController;
		this.mFlag = paramString;
		setupViews();
	//	this.positionInfo = SpotInfo.getInstance();

		this.mPopuLeft = popLeftView;
		this.mPopuRight = popRightView;
		this.mBitmapLeft = this.convertViewToBitmap(popLeftView);
		this.mBitmapRight = this.convertViewToBitmap(popRightView);

		pop = new PopupOverlay(mMapView, new PopupClickListener() {

			@Override
			public void onClickedPopup(int index) {

				if (index == 2) {
					 
					if (mContext != null)
						((CarActivity) mContext).finish();
				}
				if (index == 0) {
					if (!mFlag.equals("car")) {
						return;
					}
				//	((CarActivity) mContext).LockCar();
				}
			}
		});

	}

	 

	public void onClick(View paramView) {
		switch (paramView.getId()) {
		case R.id.map_share:
			//if (this.mContext != null)
			//	((LongClickAbleMapActivity) this.mContext).StartShareActivity();
			// ((CarActivity)this.mContext).mHandler.sendEmptyMessage(1001);
			break;
		case R.id.map_toGPS:
			if (!this.mFlag.equals("car")) {
				return;
			}
			//((CarActivity) this.mContext).LockCar();

			break;
		default:
			break;
		}
	 
	}

	public static Bitmap convertViewToBitmap(View view) {
		view.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
				MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));

	 

		view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
		view.buildDrawingCache();
		Bitmap bitmap = view.getDrawingCache();

		return bitmap;
	}

	protected boolean onTap(int paramInt) {
		if (this.mContext == null)
			return super.onTap(paramInt);

		// Toast.makeText(this.mContext, "ontap", Toast.LENGTH_SHORT).show();

		OverlayItem localOverlayItem = (OverlayItem) this.getItem(paramInt);
		// setFocus(localOverlayItem);
		/*
		 * MapView.LayoutParams localLayoutParams = (MapView.LayoutParams)
		 * this.mPopuView .getLayoutParams(); localLayoutParams.x = (-2 +
		 * this.mMark.getBounds().centerX()); localLayoutParams.y =
		 * (-this.mMark.getBounds().height()); localLayoutParams.point =
		 * localOverlayItem.getPoint();
		 * 
		 * this.mPopuView.setVisibility(0);
		 * 
		 * this.mMapView.updateViewLayout(this.mPopuView, localLayoutParams);
		 */
		if(localOverlayItem == null || localOverlayItem.getPoint() == null) {
			return super.onTap(paramInt);
		}
		this.mController.animateTo(localOverlayItem.getPoint());
		this.title.setText(localOverlayItem.getTitle());
		
	//	this.snippet.setText(this.address);
	//	this.snippet.setText("位置正在获取");
	/*	this.positionInfo.EmptyData();
		this.positionInfo.title = localOverlayItem.getTitle();
		this.positionInfo.point = localOverlayItem.getPoint();
		((LongClickAbleMapActivity) this.mContext).SearchGeoAddr(
				this.positionInfo.point, this.m_handler);*/
		// new GetAddressThread(this.m_handler, 1010,
		// localLayoutParams.point).start();
		/*
		 * this.mPopuView.findViewById(R.id.map_share).setOnClickListener(this);
		 * this.mPopuView.findViewById(R.id.map_bubblebtn)
		 * .setOnClickListener(this);
		 * 
		 * this.mPopuView.findViewById(R.id.map_toGPS).setOnClickListener(this);
		 * 
		 * this.mMapView.refresh();
		 */
		Bitmap popbitmap = convertViewToBitmap(this.mPopuView);
		this.mBitmapLeft = convertViewToBitmap(this.mPopuLeft);
		this.mBitmapRight = convertViewToBitmap(this.mPopuRight);

		Bitmap[] bmps = new Bitmap[3];

	 
			bmps[0] =    this.mBitmapLeft;
			bmps[1] =  popbitmap;//BitmapFactory.decodeStream(mContext.getAssets().open("pop_left.png"));
			bmps[2] =    this.mBitmapRight;
		 

		mPoint = getItem(paramInt).getPoint();

		pop.showPopup(bmps, getItem(paramInt).getPoint(), 32);

		/*
		 * if (null == mToast) mToast = Toast.makeText(mContext,
		 * getItem(paramInt).getTitle(), Toast.LENGTH_SHORT); else
		 * mToast.setText(getItem(paramInt).getTitle()); mToast.show();
		 */

		return super.onTap(paramInt);
	}

	/*
	 * public boolean onTouchEvent(MotionEvent paramMotionEvent, MapView
	 * paramMapView) { // if(this.mContext != null) //
	 * this.mContext.hidePopView(); return super.onTouchEvent(paramMotionEvent,
	 * paramMapView); }
	 */

	public boolean onTap(GeoPoint paramGeoPoint, MapView paramMapView) {
		// if (this.mPopuView != null)
		// this.mPopuView.setVisibility(View.GONE);

		if (pop != null) {
			pop.hidePop();
		}
		return super.onTap(paramGeoPoint, paramMapView);
	}

	public void setupViews() {
		
		this.title = ((TextView) this.mPopuView
				.findViewById(R.id.map_bubbleTitle));
	}

 
}