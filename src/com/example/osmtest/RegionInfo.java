package com.example.osmtest;

import net.tsz.afinal.annotation.sqlite.Table;

@Table(name = "region")
public class RegionInfo {
	private int id;
	private String gps;
	private String gpsbd;
	private String name;
	private String disp;
	private String tel;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getGps() {
		return gps;
	}
	public void setGps(String gps) {
		this.gps = gps;
	}
	public String getGpsbd() {
		return gpsbd;
	}
	public void setGpsbd(String gpsbd) {
		this.gpsbd = gpsbd;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDisp() {
		return disp;
	}
	public void setDisp(String disp) {
		this.disp = disp;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	
	
	
}
