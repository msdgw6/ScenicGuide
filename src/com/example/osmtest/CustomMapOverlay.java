package com.example.osmtest;
 

import org.osmdroid.views.MapView;
import org.osmdroid.views.MapView.Projection;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
/**
 * �Զ���ͼ���ʹ��
 * 
 * @Title: 
 * @Description: ʵ��TODO
 * @Copyright:Copyright (c) 2011
 * @Company:
 * @Date:2012-8-24
 * @author  longgangbai
 * @version 1.0
 */
public class CustomMapOverlay extends org.osmdroid.views.overlay.Overlay {          
	public CustomMapOverlay(Context ctx) {            
		super(ctx);             
	}          
	@Override         
	protected void draw(Canvas pC, MapView pOsmv, boolean shadow) {             
		if (shadow) {             
			return;  
		}
	    Paint lp3 = new Paint();             
		lp3.setColor(Color.RED);             
		lp3.setAntiAlias(true);             
		lp3.setStyle(Style.STROKE);             
		lp3.setStrokeWidth(1);             
		lp3.setTextAlign(Paint.Align.LEFT);             
		lp3.setTextSize(12);             
		final Rect viewportRect = new Rect();            
		final Projection projection = pOsmv.getProjection();             
		viewportRect.set(projection.getScreenRect());             // Draw a line from one corner to the other             
		pC.drawLine(viewportRect.left, viewportRect.top,                     
	    viewportRect.right, viewportRect.bottom, lp3);         
	}      
}      