package com.example.osmtest;

import java.util.List;

import net.tsz.afinal.FinalDb;

import org.gl.GlobalVariable;
import org.kobjects.base64.Base64;
import org.osmdroid.tileprovider.constants.OpenStreetMapTileProviderConstants;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.map.LocationData;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationOverlay;
import com.baidu.mapapi.map.OverlayItem;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.chuangxu.tourguide.resource.InitResourcer;
import com.chuangxu.tourguide.ui.R;

 
 

public class CarActivity extends Activity implements View.OnClickListener {

	private MapView m_mapView;
	private Drawable marker;
	private View mPopuView;
	private View mPopuLeftView;
	private View mPopuRightView;

	private GeoPoint mGeoCenter = null;
	private List<RegionInfo> mListMap;
	private TextView mTitle;
	private LocationClient mLocationClient;
	private BDLocationListener m_LocationListener = new MyLocationListener();
	MyLocationOverlay myLocationOverlay = null;
	LocationData locData = null;
	
	public Handler mHandler = new Handler() {
		public void handleMessage(Message paramMessage) {
			switch (paramMessage.what) {
			default:
				break;
			case 1001:
				if (mGeoCenter != null && m_mapView != null
						&& m_mapView.getController() != null) {
					try {
						m_mapView.getController().animateTo(mGeoCenter);
					} catch (Exception e) {
						e.printStackTrace();
						Log.w("CarActivity",
								"null GeoCenter or mapView Exception");
					}

				}
				break;
			case 1002:
				doGetLocation();
				break;
			}
		}
	};

	@Override
	public void onClick(View paramView) {
		// TODO Auto-generated method stub
		switch (paramView.getId()) {
		case R.id.map_share_home:
			this.m_mapView.getOverlays().clear();

			finish();
			break;
		case R.id.iv_user_edit:
			mHandler.sendEmptyMessage(1002);
			break;
		}
	}

	/*
	 * @Override protected boolean isRouteDisplayed() { // TODO Auto-generated
	 * method stub return false; }
	 */
	@Override
	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.map_pt_view);

		this.mPopuView = getLayoutInflater().inflate(R.layout.popup_center,
				null);
		this.mPopuLeftView = getLayoutInflater().inflate(R.layout.popup_left,
				null);
		this.mPopuRightView = getLayoutInflater().inflate(R.layout.popup_right,
				null);

		this.marker = getResources().getDrawable(R.drawable.marker_default);

		GlobalVariable app = (GlobalVariable) this.getApplication();
		if (app.mBMapMan == null) {
			app.mBMapMan = new BMapManager(getApplication());
			app.mBMapMan.init(app.mStrKey,
					new GlobalVariable.MyGeneralListener());
		}

		this.m_mapView = (MapView) findViewById(R.id.mapView_mapptview);
		this.m_mapView.setBuiltInZoomControls(false);
		this.m_mapView.getController().setZoom(8);
		
		

		this.m_mapView.addView(this.mPopuView, new MapView.LayoutParams(-2, -2,
				null, 81));
		this.mPopuView.setVisibility(8);
		this.m_mapView.addView(this.mPopuLeftView, new MapView.LayoutParams(-2,
				-2, null, 81));
		this.mPopuLeftView.setVisibility(8);
		this.m_mapView.addView(this.mPopuRightView, new MapView.LayoutParams(
				-2, -2, null, 81));
		this.mPopuRightView.setVisibility(8);

		// this.m_mapView.setOnTouchListener(this.longClickListener);

		// PositionBaseInfo baseInfo = PositionBaseInfo.getInstance();

		FinalDb db = FinalDb.create(this,
				OpenStreetMapTileProviderConstants.OSMDROID_PATH + "/"
						+ InitResourcer.map_db_name);
		mListMap = db.findAll(RegionInfo.class);

		int flag = getIntent().getFlags();

		CarItemizedOverlay Overlay = new CarItemizedOverlay(this, this.marker,
				this.mPopuView, this.mPopuLeftView, this.mPopuRightView,
				this.m_mapView, this.m_mapView.getController(), "carView");

		 
		for (int i = 0; i < mListMap.size(); i++) {
			RegionInfo localA = mListMap.get(i);

			String[] strSplit1 = localA.getGpsbd().split(",");

			double leftLongitude = Double.parseDouble(new String(Base64
					.decode(strSplit1[0])));
			double leftLatitude = Double.parseDouble(new String(Base64
					.decode(strSplit1[1])));

			GeoPoint geopoint = new GeoPoint((int) (leftLatitude * 1e6),
					(int) (leftLongitude * 1e6));

			if (i == 0)
				mGeoCenter = geopoint;

			OverlayItem localOverlayItem = new OverlayItem(geopoint,
					localA.getName(), "");
			Overlay.addItem(localOverlayItem);

		}
		this.m_mapView.getOverlays().add(Overlay);
		
		myLocationOverlay = new MyLocationOverlay(m_mapView);
		locData = new LocationData();
	    myLocationOverlay.setData(locData);
	    m_mapView.getOverlays().add(myLocationOverlay);
		myLocationOverlay.enableCompass();
		if(mGeoCenter != null) {
			this.m_mapView.getController().setCenter(mGeoCenter);
		// this.m_mapView.getController().setCenter(
		// geoCenter);
		}
		
		this.m_mapView.refresh();

		

		View goback = ((View) findViewById(R.id.map_share_home));
		goback.setOnClickListener(this);

		goback = ((View) findViewById(R.id.iv_user_edit));
		goback.setOnClickListener(this);

		mLocationClient = new LocationClient(this.getApplicationContext()); 
		
		mLocationClient = new LocationClient(this.getApplicationContext());     //����LocationClient��

		
	    
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);
		option.setAddrType("all");//���صĶ�λ�����ַ��Ϣ
		/*
		 * ���ع��־�γ�����ϵ coor=gcj02 
		 * ���ذٶ�ī�������ϵ coor=bd09 
		 * ���ذٶȾ�γ�����ϵ coor=bd09ll
		 * 
		 */
		option.setCoorType("bd09ll");//���صĶ�λ����ǰٶȾ�γ��,Ĭ��ֵgcj02
		option.setScanSpan(5000);//���÷���λ����ļ��ʱ��Ϊ5000ms
	//	option.disableCache(true);//��ֹ���û��涨λ
	//	option.setPoiNumber(5);	//��෵��POI����	
	//	option.setPoiDistance(1000); //poi��ѯ����		
	//	option.setPoiExtraInfo(true); //�Ƿ���ҪPOI�ĵ绰�͵�ַ����ϸ��Ϣ		
		mLocationClient.setLocOption(option);
		
		mLocationClient.registerLocationListener( m_LocationListener );    //ע�������
	    //mLocationClient.start();
	    
	    InitGps();
	    
	}

	@Override
	protected void onPause() {

		Log.d("mapViewPt", "onPause");

		if (this.m_mapView != null)
			this.m_mapView.onPause();
		GlobalVariable app = (GlobalVariable) this.getApplication();
		app.mBMapMan.stop();

		super.onPause();

	}

	@Override
	protected void onDestroy() {
		if (this.m_mapView != null)
			m_mapView.destroy();

		super.onDestroy();
	}

	@Override
	protected void onResume() {

		this.m_mapView.onResume();
		GlobalVariable app = (GlobalVariable) this.getApplication();
		// app.mBMapMan.start();
		if (app.mBMapMan != null) {
			app.mBMapMan.start();
		}

		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				mHandler.sendEmptyMessage(1001);
			}

		}, 2000);

		super.onResume();
	}

	public void SearchGeoAddr(GeoPoint pt, Handler hd) {

	}

	private void doGetLocation() {
		mLocationClient.start();
		int iret = mLocationClient.requestLocation();
		Log.d("location", String.format("%d", iret));

	}

	private void doStopLocation() {
		mLocationClient.stop();
	}
	
	public class MyLocationListener implements BDLocationListener {
		public void onLocationChanged(BDLocation location) {
			// TODO Auto-generated method stub
			if (location == null || m_mapView == null) {
				return;
			}
			
			 locData.latitude = location.getLatitude();
	            locData.longitude = location.getLongitude();
	            locData.accuracy = location.getRadius();
	            locData.direction = location.getDerect();
	            myLocationOverlay.setData(locData);
	            m_mapView.refresh();
	            
	            try {
	            	m_mapView.getController().animateTo(new GeoPoint((int)(locData.latitude* 1e6), (int)(locData.longitude *  1e6)), mHandler.obtainMessage(1));
	            } catch (Exception e) {
					e.printStackTrace();
					Log.w("CarActivity",
							"null GeoCenter or mapView Exception");
				}
	            /*
				GeoPoint pt = new GeoPoint(
						(int) (location.getLatitude() * 1e6),
						(int) (location.getLongitude() * 1e6));

				 if(m_mapView != null) {
				 m_mapView.getController()
							.animateTo(pt);
				 }*/
				 

				doStopLocation();
			

		}

		@Override
		public void onReceiveLocation(BDLocation location) {
			if (location == null)
				return;
			StringBuffer sb = new StringBuffer(256);
			sb.append("time : ");
			sb.append(location.getTime());
			sb.append("\nerror code : ");
			sb.append(location.getLocType());
			sb.append("\nlatitude : ");
			sb.append(location.getLatitude());
			sb.append("\nlontitude : ");
			sb.append(location.getLongitude());
			sb.append("\nradius : ");
			sb.append(location.getRadius());
			if (location.getLocType() == BDLocation.TypeGpsLocation) {
				sb.append("\nspeed : ");
				sb.append(location.getSpeed());
				sb.append("\nsatellite : ");
				sb.append(location.getSatelliteNumber());
			} else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {
				sb.append("\naddr : ");
				sb.append(location.getAddrStr());
			}

			onLocationChanged(location);
			// logMsg(sb.toString());
		}

		public void onReceivePoi(BDLocation poiLocation) {
			if (poiLocation == null) {
				return;
			}
			StringBuffer sb = new StringBuffer(256);
			sb.append("Poi time : ");
			sb.append(poiLocation.getTime());
			sb.append("\nerror code : ");
			sb.append(poiLocation.getLocType());
			sb.append("\nlatitude : ");
			sb.append(poiLocation.getLatitude());
			sb.append("\nlontitude : ");
			sb.append(poiLocation.getLongitude());
			sb.append("\nradius : ");
			sb.append(poiLocation.getRadius());
			if (poiLocation.getLocType() == BDLocation.TypeNetWorkLocation) {
				sb.append("\naddr : ");
				sb.append(poiLocation.getAddrStr());
			}
			if (poiLocation.hasPoi()) {
				sb.append("\nPoi:");
				sb.append(poiLocation.getPoi());
			} else {
				sb.append("noPoi information");
			}
			// logMsg(sb.toString());
		}
	}
	
	private void InitGps() {
		if (!checkGPSOpened(this))
			openGPSPrompt(this);

	}
	
	public static boolean checkGPSOpened(Context paramContext) {
		LocationManager localLocationManager = (LocationManager) paramContext
				.getSystemService("location");
		if (localLocationManager != null) {
			if (localLocationManager.isProviderEnabled("gps")) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}

	}
	
	public static void openGPSPrompt(final Context paramContext) {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
		localBuilder.setIcon(android.R.drawable.ic_dialog_alert);
		localBuilder.setTitle(R.string.map_gps_setting);
		localBuilder.setMessage(paramContext.getResources().getString(
				R.string.map_check_gpsopened));
		localBuilder.setPositiveButton(R.string.map_gps_setting,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
							int paramInt) {
						startGPSSetting(paramContext);
						paramDialogInterface.dismiss();
					}
				});
		localBuilder.setNegativeButton(R.string.cancle,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
							int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.show();
	}
	
	public static void startGPSSetting(Context paramContext) {
		paramContext.startActivity(new Intent(
				"android.settings.LOCATION_SOURCE_SETTINGS"));
	}
}