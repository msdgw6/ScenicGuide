package com.example.osmtest;

import net.tsz.afinal.annotation.sqlite.Table;

@Table(name = "spot")
public class SpotInfo {
	private int id;
	private int minlevel;
	private String gps;
	private String name;
	private int regionid;
	private String mp3;
	private String disp;
	private String gpsbd;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMinlevel() {
		return minlevel;
	}

	public void setMinlevel(int minlevel) {
		this.minlevel = minlevel;
	}

	public String getGps() {
		return gps;
	}

	public void setGps(String gps) {
		this.gps = gps;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRegionid() {
		return regionid;
	}

	public void setRegionid(int regionid) {
		this.regionid = regionid;
	}

	public String getMp3() {
		return mp3;
	}

	public void setMp3(String mp3) {
		this.mp3 = mp3;
	}

	public String getDisp() {
		return disp;
	}

	public void setDisp(String disp) {
		this.disp = disp;
	}

	public String getGpsbd() {
		return gpsbd;
	}

	public void setGpsbd(String gpsbd) {
		this.gpsbd = gpsbd;
	}

}
