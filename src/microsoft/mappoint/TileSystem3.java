package microsoft.mappoint;

/*
 * http://msdn.microsoft.com/en-us/library/bb259689.aspx
 *
 * Copyright (c) 2006-2009 Microsoft Corporation.  All rights reserved.
 *
 *
 */

import java.util.HashMap;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.util.constants.MapViewConstants;

import android.graphics.Point;
import android.util.Log;

/**
 * This class provides methods to handle the Mercator projection that is used for the osmdroid tile
 * system.
 */
public final class TileSystem3 {

	protected static int mTileSize = 256;
	
	public static   double MinLatitude = 0;//30.352028;
	public static   double MaxLatitude = 0.150558;//30.502586;
	public static   double MinLongitude = 0;//114.306641
	public static   double MaxLongitude = 0.186326;//114.492967
	
	public static double LeftTopLatitude;
	public static double LeftTopLongitude;
	
	public static int MaxLevel = 2;
	
/*	private static final double EarthRadius = 6378137;

*/
	static public class MapSystemInfo{
		//public double latzoom;//一纬度的比例
		//public double longzoom;//一经度的比例
		public int x;//long
		public int y;//lat
		public double z;//缩小倍数
	};
	private static HashMap<Integer,MapSystemInfo> mMapSystem = null;
	
	/*  static {
		if(mMapSystem == null)  {
			mMapSystem = new HashMap<Integer,MapSystemInfo>();
			MapSystemInfo info = new MapSystemInfo();
			info.x = 512;
			info.y = 405;
			info.z = 8;
			mMapSystem.put(0, info);
			
			info = new MapSystemInfo();
			info.x = 2048;
			info.y = 1619;
			info.z = 2;
			mMapSystem.put(1, info);
			
			info = new MapSystemInfo();
			info.x = 4096;
			info.y = 3238;
			info.z = 1;
			mMapSystem.put(2, info);
		}
	}*/
	  
	public static void setMapBound(double minLat,double maxLat, double minLong, double maxLong,int maxLevel) {
		MinLatitude = 0;
		MaxLatitude = maxLat - minLat;
		MinLongitude = 0;
		MaxLongitude = maxLong - minLong;
		
		LeftTopLatitude = maxLat;
		LeftTopLongitude = minLong;
		
		MaxLevel = maxLevel;
	}
	  
	  public static void setMapSystem(int level,MapSystemInfo info) {
		  if(mMapSystem == null)  {
			  mMapSystem = new HashMap<Integer,MapSystemInfo>();
		  }
		  
		  mMapSystem.put(level, info);
	  }
	  
	  public static double getZoomDiff(final int levelOfDetail) {
		  return mMapSystem.get(levelOfDetail).z;
	  }
	
	public static void setTileSize(final int tileSize) {
		mTileSize = tileSize;
	}

	public static int getTileSize() {
		return mTileSize;
	}

	public static int MapSize(final int levelOfDetail) {
		MapSystemInfo info = mMapSystem.get(levelOfDetail);
		
		//double db = (MaxLongitude-MinLongitude)*info.longzoom*(MaxLatitude-MinLatitude)*info.latzoom+0.5;
		if(info == null)
			return 256;
		return info.x;
	}
	
 
	private static double Clip(final double n, final double minValue, final double maxValue) {
	 
		return Math.min(Math.max(n, minValue), maxValue);
	}



 
	public static Point LatLongToPixelXY(double latitude, double longitude,
			final int levelOfDetail, final Point reuse) {
		final Point out = (reuse == null ? new Point() : reuse);
		MapSystemInfo info = mMapSystem.get(levelOfDetail);
		if(levelOfDetail == 1)
		{
			int ss = info.x;
			int jj = ss;
		}
		
		 
		
		final int mapSize = MapSize(levelOfDetail);
		
		latitude = Clip(latitude, MinLatitude, MaxLatitude);
		longitude = Clip(longitude, MinLongitude, MaxLongitude);
		
		final double x = longitude * info.x / (MaxLongitude - MinLongitude);
		final double y = latitude * info.y / (MaxLatitude - MinLatitude);
		out.x = (int) Clip(x  + 0.5, 0, mapSize - 1);
		out.y = (int) Clip(y  + 0.5, 0, mapSize - 1);
		
		return out;
	}

	 
	public static GeoPoint PixelXYToLatLong(final int pixelX, final int pixelY,
			final int levelOfDetail, final GeoPoint reuse) {
		final GeoPoint out = (reuse == null ? new GeoPoint(0, 0) : reuse);
 
		final double mapSize = MapSize(levelOfDetail);
		final double x = pixelX;//(Clip(pixelX, 0, mapSize - 1) / mapSize) - 0.5;
		final double y = pixelY;//0.5 - (Clip(pixelY, 0, mapSize - 1) / mapSize);
		
		MapSystemInfo info = mMapSystem.get(levelOfDetail);
		
		  double longitude = x / info.x * (MaxLongitude - MinLongitude);
		  double latitude = y / info.y * (MaxLatitude - MinLatitude);
		
		longitude = Clip(longitude,MinLongitude,MaxLongitude);
		latitude = Clip(latitude,MinLatitude,MaxLatitude);
		
		out.setLatitudeE6((int) (latitude * 1E6));
		out.setLongitudeE6((int) (longitude * 1E6));

		return out;
	}

	 
	public static Point PixelXYToTileXY(final int pixelX, final int pixelY, final Point reuse) {
		final Point out = (reuse == null ? new Point() : reuse);

		out.x = pixelX / mTileSize;
		out.y = pixelY / mTileSize;
		
		if(out.x < 0 || out.y < 0)
		{
			Log.d("error", "x | y < 0");
		}
		return out;
	}

	 
	public static Point TileXYToPixelXY(final int tileX, final int tileY, final Point reuse) {
		final Point out = (reuse == null ? new Point() : reuse);

		out.x = tileX * mTileSize;
		out.y = tileY * mTileSize;
		return out;
	}

	public static double GroundResolution(double latitude, final int levelOfDetail) {
		return 0.2;
		/*latitude = Clip(latitude, MinLatitude, MaxLatitude);
		return Math.cos(latitude * Math.PI / 180) * 2 * Math.PI * EarthRadius
				/ MapSize(levelOfDetail);*/
	}
	 
}
