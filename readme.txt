工程结构简介：
/src 为JAVA源代码
/generated 为ICE服务端调用代码 （自动生成）
/libs 为本地库 包括音视频解码 渲染模块 通过JNI于JAVA配合使用
/res 工程资源
/androidmanifest.xml 为工程结构描述文件
/doc 为工程的JDOC
/doc/document_rnbs 为工程的设计文档 包括UI 解码 业务逻辑 需求分析

版本演变历史：
浏览SVN提交记录
和测试文档

移动终端(设备名称：现代T7)最佳网络流视频参数设置

HDMI input
Read: fix format capture
Set format: 720P,60HZ,NTSC

bitrate=2048000
framerate=20
I frame num=19
H264 encode select: Baseline Profile
H264 encode select: High Quality Mode
H264 encode standard: Level 4.0

ts output: enable
tcp stream tag output: disable
rtsp stream server: disable
disable video sclaing

AAC
audio enable=1
audio sample=48000
audio bitrate=128000